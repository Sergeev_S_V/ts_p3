import React, {Component} from 'react';
import {withRouter, Link} from 'react-router-dom';


class Page404 extends Component {

  render() {

    return (
      <div>
        <h1>404 Not Found</h1>
        <Link to="/">Go home</Link>
      </div>
    );
  }
}

export default withRouter(Page404);
