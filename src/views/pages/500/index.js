import React, {Component} from 'react';
import {withRouter, Link} from 'react-router-dom';


class Page500 extends Component {

  render() {

    return (
      <div>
        <h1>Internal Server Error</h1>
        <Link to="/">Go home</Link>
      </div>
    );
  }
}

export default withRouter(Page500);
