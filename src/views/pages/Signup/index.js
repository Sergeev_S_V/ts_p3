import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {observer, inject} from 'mobx-react';
import {pipe} from 'ramda';

import {Button, Alert, Form, Input, Icon, MaterialIcon} from '../../../components';
import {SIGN_IN_ROUTE, utils, KEYS} from "../../../internal";

import './Signup.scss';


class Signup extends Component {

  render() {
    const {
      stores: {signupStore: {isLoading}, authStore: {errors}},
      form
    } = this.props;

    return (
      <div className="sw-authentification">
        <div className="sw-authentification__inner">
          <header className="sw-authentification__header">
            <h1>Регистрация</h1>
            <p>Создать новый аккаунт</p>
          </header>

          {errors.has(KEYS.SIGNUP_ERROR) && (errors.get(KEYS.SIGNUP_ERROR).map(
            (error, idx) => (
              <Alert color='warning' key={idx}>
                {error}
              </Alert>
            )
          ))}

          <div className="sw-authentification__body">
            <Form onSubmit={(e) => form.onSubmit(e)}>
              <Form.Group>
                <Input placeholder={form.$('email').placeholder}
                       name={form.$('email').name}
                       value={form.$('email').value}
                       onChange={(e) => form.$('email').onChange(e)}/>
                {form.$('email').hasError && (
                  <Alert color='warning'>
                    <Icon>
                      <MaterialIcon name="error_outline"/>
                    </Icon>
                    {form.$('email').error}
                  </Alert>
                )}
                {errors.has(KEYS.EMAIL_FIELD_ERROR) && errors.get(KEYS.EMAIL_FIELD_ERROR).map(
                  (error, idx) => (
                    <Alert color='warning' key={idx}>
                      <Icon>
                        <MaterialIcon name="error_outline"/>
                      </Icon>
                      {error}
                    </Alert>
                  )
                )}
              </Form.Group>
              <Form.Group>
                <Input type="password"
                       placeholder={form.$('password').placeholder}
                       name={form.$('password').name}
                       value={form.$('password').value}
                       onChange={(e) => form.$('password').onChange(e)}/>
                {form.$('password').hasError && (
                  <Alert color='warning'>
                    <Icon>
                      <MaterialIcon name="error_outline"/>
                    </Icon>
                    {form.$('password').error}
                  </Alert>
                )}
                {errors.has(KEYS.PASSWORD_FIELD_ERROR) && errors.get(KEYS.PASSWORD_FIELD_ERROR).map(
                  (error, idx) => (
                    <Alert color='warning' key={idx}>
                      <Icon>
                        <MaterialIcon name="error_outline"/>
                      </Icon>
                      {error}
                    </Alert>
                  )
                )}
              </Form.Group>
              <Form.Group>
                <Input type="password"
                       placeholder={form.$('confirmPassword').placeholder}
                       name={form.$('confirmPassword').name}
                       value={form.$('confirmPassword').value}
                       onChange={(e) => form.$('confirmPassword').onChange(e)}/>
                {form.$('confirmPassword').hasError && (
                  <Alert color='warning'>
                    <Icon>
                      <MaterialIcon name="error_outline"/>
                    </Icon>
                    {form.$('confirmPassword').error}
                  </Alert>
                )}
                {errors.has(KEYS.CONFIRM_PASSWORD_FIELD_ERROR) && errors.get(KEYS.CONFIRM_PASSWORD_FIELD_ERROR).map(
                  (error, idx) => (
                    <Alert color='warning' key={idx}>
                      <Icon>
                        <MaterialIcon name="error_outline"/>
                      </Icon>
                      {error}
                    </Alert>
                  )
                )}
              </Form.Group>
              <Button type='submit'
                      fluid
                      loading={isLoading}
                      disabled={isLoading}>
                Регистрация
              </Button>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

const applyWrappers = pipe(observer, inject('stores'), withRouter);

const Wrapper = (props) => {
  const {stores: {authStore, signupStore}, history} = props;

  const hooks = {
    onSuccess(form) {
      signupStore.toggleLoading();
      authStore.signUp(form.values()).then(
        () => {
          signupStore.toggleLoading();
          history.push(SIGN_IN_ROUTE);
        },
        () => {
          signupStore.toggleLoading();
        }
      );
    }
  };

  const fields = [{
    name: 'email',
    placeholder: 'Электронная почта',
    rules: 'required|email|string',
    value: ''
  }, {
    name: 'password',
    placeholder: 'Пароль',
    rules: 'required|string|between:6,32|regex:/(?=.*[a-z])+(?=.*[A-Z])+(?=.*[0-9])+(?=.*[!@#\\$%^&~`])+/',
    value: ''
  }, {
    name: 'confirmPassword',
    placeholder: 'Подтвердите пароль',
    rules: 'required|string|same:password',
    value: ''
  }];

  const form = utils.createForm({fields}, {hooks});

  const Wrapped = applyWrappers(Signup);

  return (<Wrapped form={form}/>);
};

export default applyWrappers(Wrapper);
