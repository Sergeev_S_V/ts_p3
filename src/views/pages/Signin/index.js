import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {observer, inject} from 'mobx-react';
import {pipe} from 'ramda';

import './Signin.scss';
import {Button, Alert, Form, Input, Icon, MaterialIcon} from '../../../components';
import {utils, ROOT_ROUTE, SIGN_UP_ROUTE, KEYS} from '../../../internal';


class Signin extends Component {

  render() {
    const {
      stores: {signinStore: {isLoading}, authStore: {errors}},
      form
    } = this.props;

    return (
      <div className="sw-authentification">
        <div className="sw-authentification__inner">
          <header className="sw-authentification__header">
            <h1>Вход на сайт</h1>
            <p>Использовать учетную запись для входа в систему</p>
          </header>

          {errors.has(KEYS.SIGNIN_ERROR) && (<Alert color='warning'>{errors.get(KEYS.SIGNIN_ERROR)}</Alert>)}

          <div className="sw-authentification__body">
            <Form onSubmit={(e) => form.onSubmit(e)}>
              <Form.Group>
                <Input placeholder={form.$('UserName').placeholder}
                       name={form.$('UserName').name}
                       value={form.$('UserName').value}
                       onChange={(e) => form.$('UserName').onChange(e)}/>
                {form.$('UserName').hasError && (
                  <Alert color='warning'>
                    <Icon>
                      <MaterialIcon name="error_outline"/>
                    </Icon>
                    {form.$('UserName').error}
                  </Alert>
                )}
              </Form.Group>
              <Form.Group>
                <Input type="Password"
                       placeholder={form.$('Password').placeholder}
                       name={form.$('Password').name}
                       value={form.$('Password').value}
                       onChange={(e) => form.$('Password').onChange(e)}/>
                {form.$('Password').hasError && (
                  <Alert color='warning'>
                    <Icon>
                      <MaterialIcon name="error_outline"/>
                    </Icon>
                    {form.$('Password').error}
                  </Alert>
                )}
              </Form.Group>
              <Button type='submit'
                      fluid
                      loading={isLoading}
                      disabled={isLoading}>
                Войти
              </Button>
              <Link to={SIGN_UP_ROUTE} className="registration-link">Регистрация</Link>
            </Form>

            <div className="sw-outer-authentification">
              <p className="sw-outer-authentification__header">
                Используйте другой сервис для входа в систему
              </p>
              <div className="sw-outer-authentification__body">
                <div className="sw-outer-links">
                  <div className="sw-outer-links__item">
                    <a href="#" className="sw-google-link">
                      <svg width="25" height="25" viewBox="0 0 512 512" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M113.47 309.408L95.648 375.94L30.509 377.318C11.042 341.211 0 299.9 0 256C0 213.549 10.324 173.517 28.624 138.268H28.638L86.63 148.9L112.034 206.544C106.717 222.045 103.819 238.685 103.819 256C103.821 274.792 107.225 292.797 113.47 309.408Z"
                          fill="#FBBB00"/>
                        <path
                          d="M507.527 208.176C510.467 223.662 512 239.655 512 256C512 274.328 510.073 292.206 506.402 309.451C493.94 368.134 461.377 419.376 416.268 455.638L416.254 455.624L343.21 451.897L332.872 387.362C362.804 369.808 386.196 342.337 398.518 309.451H261.628V208.176H400.515H507.527Z"
                          fill="#518EF8"/>
                        <path
                          d="M416.253 455.624L416.267 455.638C372.396 490.901 316.666 512 256 512C158.509 512 73.748 457.509 30.509 377.319L113.47 309.409C135.089 367.107 190.748 408.18 256 408.18C284.047 408.18 310.323 400.598 332.87 387.362L416.253 455.624Z"
                          fill="#28B446"/>
                        <path
                          d="M419.404 58.936L336.471 126.832C313.136 112.246 285.552 103.82 256 103.82C189.271 103.82 132.571 146.777 112.035 206.544L28.638 138.268H28.624C71.23 56.123 157.06 0 256 0C318.115 0 375.068 22.126 419.404 58.936Z"
                          fill="#F14336"/>
                      </svg>
                      <span className="sw-google-link__text">
                      Google
                    </span>
                    </a>
                  </div>
                  <div className="sw-outer-links__item">
                    <a href="#" className="sw-facebook-link">
                      <svg width="25" height="25" viewBox="0 0 292 292" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clipPath="url(#clip0)">
                          <path
                            d="M145.659 0C226.109 0 291.319 65.219 291.319 145.66C291.319 226.11 226.109 291.319 145.659 291.319C65.209 291.319 0 226.109 0 145.66C0 65.219 65.21 0 145.659 0Z"
                            fill="#3B5998"/>
                          <path
                            d="M163.394 100.277H182.166V72.547H160.099V72.647C133.361 73.594 127.881 88.624 127.398 104.41H127.343V118.257H109.136V145.413H127.343V218.206H154.782V145.413H177.259L181.601 118.257H154.791V109.891C154.791 104.556 158.341 100.277 163.394 100.277Z"
                            fill="white"/>
                        </g>
                        <defs>
                          <clipPath id="clip0">
                            <rect width="291.319" height="291.319" fill="white"/>
                          </clipPath>
                        </defs>
                      </svg>
                      <span className="sw-facebook-link__text">
                      Facebook
                    </span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const applyWrappers = pipe(observer, inject('stores'), withRouter);

const Wrapper = (props) => {
  const {stores: {authStore, signinStore}, history} = props;

  const hooks = {
    onSuccess(form) {
      const formData = form.values();
      formData.grant_type = "password";
      signinStore.toggleLoading();
      authStore.signIn(utils.encodeQueryString(formData)).then(
        () => {
          signinStore.toggleLoading();
          history.push(ROOT_ROUTE);
        },
        () => {
          signinStore.toggleLoading();
        }
      );
    }
  };
  const fields = [{
    name: 'UserName',
    placeholder: 'Электронная почта',
    rules: 'required|email|string',
    value: '',
  }, {
    name: 'Password',
    placeholder: 'Пароль',
    rules: 'required|string|between:6,32|regex:/(?=.*[a-z])+(?=.*[A-Z])+(?=.*[0-9])+(?=.*[!@#\\$%^&~`])+/',
    value: '',
  }];

  const form = utils.createForm({fields}, {hooks});

  const Wrapped = applyWrappers(Signin);

  return (<Wrapped form={form}/>);
};

export default applyWrappers(Wrapper);
