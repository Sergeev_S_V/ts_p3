import React from 'react';
import {withRouter} from "react-router-dom";
import {pipe} from "ramda";
import {getSnapshot} from "mobx-state-tree";
import {inject, observer} from "mobx-react";
import classNames from "classnames";
import Rating from "react-rating";

import {
  Column,
  Container,
  Row,
  Title,
  Card,
  SubTitle,
  MaterialIcon,
  Rank,
  Icon,
  Form,
  Label,
  Input,
  Button,
  CustomInput
} from "../../../components";

import {
  PROBLEM_LEVEL_EASY,
  PROBLEM_LEVEL_HARD,
  PROBLEM_LEVEL_MEDIUM
} from "../../../internal";

import "./Problems.scss";


class Problems extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      addOpen: true
    };

    this.toggleAdd = this.toggleAdd.bind(this);
  }

  componentDidMount() {
    const {
      problemStore,
      controlProblemUIStore
    } = this.props.stores;

    problemStore.getProblems(getSnapshot(
      controlProblemUIStore.filter
    ));
  }

  toggleAdd() {
    this.setState(prevState => ({
      addOpen: !prevState.addOpen
    }));
  }

  render() {
    const {addOpen} = this.state;

    const {
      stores: {
        problemStore: {problems},
      }
    } = this.props;

    return (
      <Container>
        <header className="p-index__heading p-index__heading--flexed">
          <Title className="t-primary-headings">Задачи</Title>
          <Icon className="add-button" size={25} color="grey" onClick={this.toggleAdd}>
            <MaterialIcon name="add" className={classNames(addOpen && "material-icons--rotated")}/>
          </Icon>
        </header>
        <div className="p-index__body">
          <Form className={classNames("card-form", addOpen && "card-form--activated")}>
            <header className="card-form__heading">
              <h2 className="t-secondary-headings">Информация о задаче</h2>
            </header>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Наименование</Label>
                </Column>
                <Column fluid>
                  <Input type="text" placeholder="Введите имя"/>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Код задачи</Label>
                </Column>
                <Column fluid>
                  <Input type="text" placeholder="Введите код"/>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Уровень</Label>
                </Column>
                <Column className="align-centered" fluid>
                  <Rating
                    className="sw-rating-list rating-padding-left"
                    initialRating={0}
                    fullSymbol={<MaterialIcon name='star'/>}
                    emptySymbol={<MaterialIcon name='star_border'/>}
                  />
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Предметная область</Label>
                </Column>
                <Column fluid>
                  <Input tag="select">
                    <option>option1</option>
                    <option>option2</option>
                    <option>option3</option>
                  </Input>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Профиль безопасности</Label>
                </Column>
                <Column fluid>
                  <Input tag="select">
                    <option>option1</option>
                    <option>option2</option>
                    <option>option3</option>
                  </Input>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Похожие задачи</Label>
                </Column>
                <Column fluid>
                  <Input tag="select">
                    <option>option1</option>
                    <option>option2</option>
                    <option>option3</option>
                  </Input>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Коллекции</Label>
                </Column>
                <Column fluid>
                  <Input tag="select">
                    <option>option1</option>
                    <option>option2</option>
                    <option>option3</option>
                  </Input>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Синхронизирования проверка</Label>
                </Column>
                <Column>
                  <CustomInput type="checkbox" label="Hello"/>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Проверять имена столбцов</Label>
                </Column>
                <Column fluid>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Активно</Label>
                </Column>
                <Column fluid>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Row className="align-centered">
                <Column col={4}>
                  <Label className="center-label">Наличие сортировки</Label>
                </Column>
                <Column fluid>
                </Column>
              </Row>
            </Form.Group>
            <Form.Group>
              <Label className="align-centered">Описание</Label>
              <Input tag="textarea" rows={5}/>
            </Form.Group>
            <Form.Group>
              <Label className="align-centered">Коментарии</Label>
              <Input tag="textarea" rows={5}/>
            </Form.Group>
            <Row>
              <Column col={12}>
                <Button type="button">
                  Сохранить
                </Button>
                <Button className="sw-ml-10" type="button" outlined>
                  Отменить
                </Button>
              </Column>
            </Row>
          </Form>
          {problems.map((problem, idx) => {
            return (
              <Row key={problem.Id}>
                <Column col={12}>
                  <Card inner first={idx === 0}>
                    <Card.PrimeSide>
                      <Card.Heading flexed>
                        <Title>
                          <p className='sw-title__code'>{problem.ProblemCode}</p>
                          <p className='sw-title__slash'>|</p>
                          <p className='sw-title__text'>{problem.ProblemName}</p>
                        </Title>
                        <Rank
                          color={
                            (PROBLEM_LEVEL_EASY.includes(problem.ProblemLevel) && "green") ||
                            (PROBLEM_LEVEL_MEDIUM.includes(problem.ProblemLevel) && "orange") ||
                            (PROBLEM_LEVEL_HARD.includes(problem.ProblemLevel) && "red")
                          }
                          level={problem.ProblemLevel}
                        />
                        <Rating
                          className='sw-rating-list'
                          readonly
                          initialRating={problem.Raiting}
                          fullSymbol={<MaterialIcon name='star'/>}
                          emptySymbol={<MaterialIcon name='star_border'/>}
                        />
                        <div className="sw-procent-finishing">
                          {Math.round(problem.PercentOfUsersSolved)} %
                        </div>
                      </Card.Heading>
                      <Card.Body>
                        <SubTitle>{problem.SubjectName}</SubTitle>
                      </Card.Body>
                    </Card.PrimeSide>
                    <Card.SecondarySide>
                      <Card.Heading>
                        <Card.Status>
                          {problem.IsSolved && (<Icon size={22} className='sw-m-h-7' color="green">
                            <MaterialIcon name='done_all'/>
                          </Icon>)}
                          {problem.IsFlagged && (<Icon size={22} className='sw-m-h-7' color="red">
                            <MaterialIcon name='flag'/>
                          </Icon>)}
                        </Card.Status>
                      </Card.Heading>
                      <Card.Body fixed>
                        <Card.Dropdown icon={<MaterialIcon name='more_horiz'/>}>
                          <Card.DropdownItem onClick={() => {
                          }}>
                            Редактировать
                          </Card.DropdownItem>
                        </Card.Dropdown>
                      </Card.Body>
                    </Card.SecondarySide>
                  </Card>
                </Column>
              </Row>
            );
          })}
        </div>
      </Container>
    );
  }
}

const applyWrappers = pipe(observer, inject('stores'), withRouter);

export default applyWrappers(Problems);
