import React from 'react';
import {inject, observer} from "mobx-react/index";
import {pipe} from "ramda";

import {utils} from "../../internal";
import {Button, CustomInput, Form, Input, List} from "../../components";
import {getSnapshot} from "mobx-state-tree";

const TAGS_GET_OPTIONS = 'tags-get-options';

class AddTag extends React.Component {

  // componentDidMount() {
  //   const {stores: {
  //     tagsStore: {getProblemsForTagLookUp, getLinksForTagLookUp},
  //     tagsUIStore: {toggleLoading}
  //   }} = this.props;
  //
  //   toggleLoading(ADD_TAG_SELECT);
  //   Promise.all([getProblemsForTagLookUp(), getLinksForTagLookUp()])
  //   .then(
  //     () => toggleLoading(ADD_TAG_SELECT),
  //     () => toggleLoading(ADD_TAG_SELECT)
  //   )
  // }



  render() {
    const {
      form,
      isLoading,
      problemsForTagLookUp,
      linksForTagLookUp,
      stores: {
        tagsUIStore: {loaders}
      }
    } = this.props;
    // console.log(linksForTagLookUp.map(x => ({label: x.LinkName, value: x.LinkId})), 'mapped');
    // console.log(form.$('Links').value, 'value');
    return(
      <Form onSubmit={(e) => form.onSubmit(e)}>
        <header className="p-index__heading">
          <h2 className="t-secondary-headings">Информация о тегах</h2>
        </header>
        <List flexed className='sw-m-b-15'>
          <label htmlFor='add-tag__tag-name' className='sw-width-100'>
            {form.$('TagName').label}
          </label>
          <Input
            id='add-tag__tag-name'
            type="text"
            name={form.$('TagName').name}
            placeholder={form.$('TagName').placeholder}
            value={form.$('TagName').value}
            onChange={(e) => form.$('TagName').onChange(e)}
          />
        </List>
        <List flexed className='sw-m-b-15'>
          <label htmlFor='add-tag__problems' className="sw-width-100">
            {form.$('Problems').label}
          </label>
          <CustomInput
            type='select'
            id='add-tag__problems'
            isMulti
            name={form.$('Problems').name}
            placeholder={form.$('Problems').placeholder}
            options={
              problemsForTagLookUp.map(x => ({label: x.ProblmName, value: x.ProblemId}))
            }
            onChange={selectedOptions => form.$('Problems').onChange(selectedOptions)}
            value={form.$('Problems').value}
            isLoading={loaders.get(TAGS_GET_OPTIONS)}
            isDisabled={loaders.get(TAGS_GET_OPTIONS)}
          />
        </List>
        <List flexed className='sw-m-b-15'>
          <label htmlFor='add-tag__links' className="sw-width-100">
            {form.$('Links').label}
          </label>
          <CustomInput
            type='select'
            id='add-tag__links'
            isMulti
            name={form.$('Links').name}
            placeholder={form.$('Links').placeholder}
            options={
              linksForTagLookUp.map(x => ({label: x.LinkName, value: x.LinkId}))
            }
            onChange={selectedOptions => form.$('Links').onChange(selectedOptions)}
            value={form.$('Links').value}
            isLoading={loaders.get(TAGS_GET_OPTIONS)}
            isDisabled={loaders.get(TAGS_GET_OPTIONS)}
          />
        </List>
        <List flexed className='sw-m-b-15'>
          <label htmlFor='add-tag__description' className="sw-width-100">
            {form.$('Description').label}
          </label>
          <Input
            tag='textarea'
            id="add-tag__description"
            name={form.$('Description').name}
            placeholder={form.$('Description').placeholder}
            value={form.$('Description').value}
            onChange={(e) => form.$('Description').onChange(e)}
            cols="30"
            rows="3"
          />
        </List>
        <div className='sw-form-buttons'>
          <Button
            type='submit'
            className='sw-m-0 sw-fz-14'
            loading={isLoading}
            disabled={!!isLoading}
          >
            Сохранить
          </Button>
          <Button
            className='sw-m-0 sw-m-l-10 sw-fz-14'
            outlined
            onClick={(e) => form.onClear(e)}
            disabled={!!isLoading}
          >
            Отменить
          </Button>
        </div>
      </Form>
    );
  }
}

const applyWrappers = pipe(observer, inject('stores'));

const Wrapper = (props) => {
  const {
    addTag, toggleLoading, getTags, editingTag, problemsForTagLookUp, linksForTagLookUp
  } = props;
  // console.log(editingTag);
  console.log(getSnapshot(problemsForTagLookUp));
  console.log(getSnapshot(linksForTagLookUp));
  const hooks = {
    onSuccess(form) {
      const values = form.values();
      const formData = {
        ...values,
        Problems: values.Problems.map(x => x.value),
        Links: values.Links.map(x => x.value)
      };

      toggleLoading();
      addTag(formData)
      .then(
        () => {
          toggleLoading();
          getTags();
        },
        () => toggleLoading()
      );
    }
  };



  const fields = [{
    name: 'TagName',
    placeholder: 'Введите имя',
    rules: 'required|string',
    value: editingTag && editingTag.TagName ? editingTag.TagName : '',
    label: 'Наименование тега'
  }, {
    name: 'Problems',
    placeholder: 'Выберите предметную область',
    rules: 'required|integer',
    value: editingTag && editingTag.Problems ? editingTag.Problems : [],
    label: 'Задачи'
  }, {
    name: 'Links',
    placeholder: 'Выберите коллекцию',
    rules: 'required|integer',
    value: editingTag && editingTag.Links ? editingTag.Links : [],
    label: 'Ссылки'
  }, {
    name: 'Description',
    placeholder: 'Выберите описание',
    rules: 'required|string',
    value: editingTag && editingTag.Description ? editingTag.Description : '',
    label: 'Описание'
  }];

  const form = utils.createForm({fields}, {hooks});
  const Component = applyWrappers(AddTag);

  return (<Component form={form} {...props} />);
};

export default Wrapper;
