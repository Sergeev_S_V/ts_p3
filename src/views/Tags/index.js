import React from 'react';
import classNames from "classnames";
import {inject, observer} from "mobx-react/index";
import {pipe} from "ramda";

import {
  Card, Column, Container, Icon, MaterialIcon, Row, SubTitle, Title, WithLoading
} from "../../components";
import AddTag from "./AddTag";

import './Tags.scss';
import {getSnapshot} from "mobx-state-tree";


const TAGS_COMMON_LOADING = 'tags-common-loading';
const TAGS_ADD = 'tags-add';
const TAGS_GET_OPTIONS = 'tags-get-options';


class Tags extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpened: false,
      editingTag: null
    };

    this.toggle = this.toggle.bind(this);
    this.getTags = this.getTags.bind(this);
    this.editTag = this.editTag.bind(this);
    this.deleteTag = this.deleteTag.bind(this);
  }

  componentDidMount() {
    this.getTags();
    this.getOptionsForAddTag();
  }

  getOptionsForAddTag() {
    const {stores: {
      tagsStore: {getProblemsForTagLookUp, getLinksForTagLookUp},
      tagsUIStore: {toggleLoading}
    }} = this.props;

    toggleLoading(TAGS_GET_OPTIONS);
    Promise.all([getProblemsForTagLookUp(), getLinksForTagLookUp()])
    .then(
      () => toggleLoading(TAGS_GET_OPTIONS),
      () => toggleLoading(TAGS_GET_OPTIONS)
    )
  }

  getTags() {
    const {stores: {
      tagsStore: {getTags},
      tagsUIStore: {toggleLoading}
    }} = this.props;

    toggleLoading(TAGS_COMMON_LOADING);
    getTags()
    .then(
      () => toggleLoading(TAGS_COMMON_LOADING),
      () => toggleLoading(TAGS_COMMON_LOADING)
    )
  }

  editTag(tag) {
    const {isOpened} = this.state;
    // const {stores: {
    //   tagsStore: {problemsForTagLookUp, linksForTagLookUp}
    // }} = this.props;
    //
    // console.log(getSnapshot(tag), 'tag');
    // const editingTag = {
    //   ...tag,
    //   Problems: problemsForTagLookUp.map(x => ({label: x.ProblmName, value: x.ProblemId})),
    //   Links: linksForTagLookUp.map(x => ({label: x.LinkName, value: x.LinkId}))
    // };
    // console.log(editingTag, 'editingTag');
    if (!isOpened) {
      this.toggle();
      this.setState({editingTag: tag});
    }

    this.setState({editingTag: tag});
  }

  deleteTag(id) {
    const {stores: {
      tagsStore: {deleteTag},
      tagsUIStore: {toggleLoading}
    }} = this.props;

    toggleLoading(TAGS_COMMON_LOADING);
    deleteTag(id)
    .then(
      () => {
        toggleLoading(TAGS_COMMON_LOADING);
        this.getTags();
      },
      () => toggleLoading(TAGS_COMMON_LOADING)
    )
  }

  toggle() {
    this.setState(prevState => ({
      isOpened: !prevState.isOpened
    }))
  }

  render() {
    const {stores: {
      tagsStore: {tags, addTag, problemsForTagLookUp, linksForTagLookUp},
      tagsUIStore: {loaders, toggleLoading}
    }} = this.props;
    const {isOpened, editingTag} = this.state;

    return(
      <Container>
        <header className='p-index__heading p-index__heading--flexed'>
          <Title className='t-primary-headings'>Теги</Title>
          <Icon className="add-button" size={25} color="grey" onClick={this.toggle}>
            <MaterialIcon name="add" className={classNames(isOpened && "material-icons--rotated")}/>
          </Icon>
        </header>
        <div className="p-index__body">
          {isOpened &&
            <AddTag
              addTag={addTag}
              getTags={this.getTags}
              toggleLoading={() => toggleLoading(TAGS_ADD)}
              isLoading={loaders.get(TAGS_ADD)}
              editingTag={editingTag}
              problemsForTagLookUp={problemsForTagLookUp}
              linksForTagLookUp={linksForTagLookUp}
            />
          }
          <WithLoading loading={loaders.get(TAGS_COMMON_LOADING)}>
            {tags.length > 0
              ? tags.map((tag, i) => (
                  <Row key={tag.Id}>
                    <Column col={12}>
                      <Card inner first={i === 0}>
                        <Card.PrimeSide>
                          <Card.Heading flexed>
                            <Title>{tag.TagName}</Title>
                          </Card.Heading>
                          <Card.Body>
                            <SubTitle>{tag.Description}</SubTitle>
                          </Card.Body>
                        </Card.PrimeSide>
                        <Card.SecondarySide>
                          <Card.Body fixed>
                            <Card.Dropdown icon={<MaterialIcon name='more_horiz'/>}>
                              <Card.DropdownItem onClick={() => this.deleteTag(tag.Id)}>
                                Удалить
                              </Card.DropdownItem>
                              <Card.DropdownItem onClick={() => this.editTag(tag)}>
                                Редактировать
                              </Card.DropdownItem>
                            </Card.Dropdown>
                          </Card.Body>
                        </Card.SecondarySide>
                      </Card>
                    </Column>
                  </Row>
                ))
              : <p>Нет результата</p>
            }
          </WithLoading>
        </div>
      </Container>
    )
  }
}

const applyWrappers = pipe(observer, inject('stores'));
export default applyWrappers(Tags);
