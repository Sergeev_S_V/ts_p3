import React from 'react';
import {withRouter} from "react-router-dom";
import {pipe} from "ramda";
import {getSnapshot} from "mobx-state-tree";
import {inject, observer} from "mobx-react";
import Rating from "react-rating";

import {
  Column,
  Container,
  Row,
  Title,
  Card,
  SubTitle,
  WithLoading,
  MaterialIcon,
  Rank,
  Icon
} from "../../components";
import {
  PROBLEM_SUBMISSION_ROUTE,
  PROBLEM_LEVEL_EASY,
  PROBLEM_LEVEL_HARD,
  PROBLEM_LEVEL_MEDIUM
} from '../../internal';


class Problems extends React.Component {

  componentDidMount() {
    const {
      stores: {
        problemStore: {getProblems},
        problemsUIStore: {toggleLoading, filter}
      }
    } = this.props;

    toggleLoading();
    getProblems(getSnapshot(filter)).then(
      () => toggleLoading(),
      () => toggleLoading()
    )
  };

  goTo = path => {
    this.props.history.push(path);
  };

  render() {
    const {
      stores: {
        problemStore: {problems},
        problemsUIStore: {isLoading}
      }
    } = this.props;

    return (
      <Container>
        <header className='p-index__heading'>
          <Title className='t-primary-headings'>Задачи</Title>
        </header>
        <div className="p-index__body">
          <WithLoading loading={isLoading}>
            {problems.length > 0
              ? problems.map((problem, i) => (
                <Row key={problem.Id}>
                  <Column col={12}>
                    <Card inner first={i === 0}>
                      <Card.PrimeSide onClick={() => this.goTo(PROBLEM_SUBMISSION_ROUTE)}>
                        <Card.Heading flexed>
                          <Title>
                            <p className='sw-title__code'>{problem.ProblemCode}</p>
                            <p className='sw-title__slash'>|</p>
                            <p className='sw-title__text'>{problem.ProblemName}</p>
                          </Title>
                          <Rank
                            color={
                              (PROBLEM_LEVEL_EASY.includes(problem.ProblemLevel) && "green") ||
                              (PROBLEM_LEVEL_MEDIUM.includes(problem.ProblemLevel) && "orange") ||
                              (PROBLEM_LEVEL_HARD.includes(problem.ProblemLevel) && "red")
                            }
                            level={problem.ProblemLevel}
                          />
                          <Rating
                            className='sw-rating-list'
                            readonly
                            initialRating={problem.Raiting}
                            fullSymbol={<MaterialIcon name='star'/>}
                            emptySymbol={<MaterialIcon name='star_border'/>}
                          />
                          <div className="sw-procent-finishing">
                            {Math.round(problem.PercentOfUsersSolved)} %
                          </div>
                        </Card.Heading>
                        <Card.Body>
                          <SubTitle>{problem.SubjectName}</SubTitle>
                        </Card.Body>
                      </Card.PrimeSide>
                      <Card.SecondarySide>
                        <Card.Heading>
                          <Card.Status>
                            {problem.IsSolved && (<Icon size={22} className='sw-m-l-10' color="green">
                              <MaterialIcon name='done_all'/>
                            </Icon>)}
                            {problem.IsFlagged && (<Icon size={22} className='sw-m-l-10' color="red">
                              <MaterialIcon name='flag'/>
                            </Icon>)}
                          </Card.Status>
                        </Card.Heading>
                        <Card.Body fixed>
                          <Card.Dropdown icon={<MaterialIcon name='more_horiz'/>}>
                            <Card.DropdownItem onClick={() => this.goTo(PROBLEM_SUBMISSION_ROUTE)}>
                              Решить
                            </Card.DropdownItem>
                          </Card.Dropdown>
                        </Card.Body>
                      </Card.SecondarySide>
                    </Card>
                  </Column>
                </Row>
              ))
              : <p>Задач нет</p>
            }
          </WithLoading>
        </div>
      </Container>
    );
  }
}

const applyWrappers = pipe(observer, inject('stores'), withRouter);
export default applyWrappers(Problems);


