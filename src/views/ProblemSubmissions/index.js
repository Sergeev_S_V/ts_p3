import React from 'react';
import {Link} from "react-router-dom";
import {inject, observer} from "mobx-react";
import {getSnapshot} from "mobx-state-tree";
import {pipe} from "ramda";
import moment from 'moment';
import DatePicker from "react-datepicker";

import {
  Card,
  Column,
  Container,
  MaterialIcon,
  Row,
  Title,
  WithLoading,
  Dropbox,
  SubTitle,
  Divider, PortalComponent, FilterContainer, Icon, List, Filter, Form, Button, CustomInput
} from "../../components";
import {PROBLEM_SUBMISSION_ROUTE, config} from '../../internal';

import './ProblemSubmissions.scss';


class ProblemSubmissions extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      submissionStatusId: null,
      shownFilterBodies: []
    };

    this.rootFilterNode = document.getElementById(config.MOUNT_FILTER_ID);
    this.filterContainer = document.createElement('div');

    this.toggleFilterBody = this.toggleFilterBody.bind(this);
    this.toggleStatusDropbox = this.toggleStatusDropbox.bind(this);
    this.filterStatusHandler = this.filterStatusHandler.bind(this);
    this.filterDateHandler = this.filterDateHandler.bind(this);
    this.getProblemSubmissions = this.getProblemSubmissions.bind(this);
  }

  componentDidMount() {
    this.getProblemSubmissions()
  }

  getProblemSubmissions() {
    const {
      stores: {
        problemSubmissionsStore: {getProblemSubmissions},
        problemSubmissionsUIStore: {filter, toggleLoading}
      }
    } = this.props;

    toggleLoading();
    getProblemSubmissions(getSnapshot(filter))
    .then(
      () => toggleLoading(),
      () => toggleLoading()
    );
  }

  toggleStatusDropbox = (id) => {
    const {submissionStatusId} = this.state;
    if (id !== submissionStatusId) {
      this.setState({submissionStatusId: id});
    } else {
      this.setState({submissionStatusId: null});
    }
  };

  toggleFilterBody(id) {
    const shownFilterBodies = this.state.shownFilterBodies;
    if (shownFilterBodies.includes(id)) {
      const index = shownFilterBodies.indexOf(id);
      shownFilterBodies.splice(index, 1);
    } else {
      shownFilterBodies.push(id);
    }

    this.setState({shownFilterBodies});
  }

  filterStatusHandler(key, value) {
    const {stores: {problemSubmissionsUIStore}} = this.props;

    problemSubmissionsUIStore.runInAction(() => {
      problemSubmissionsUIStore.filter[key] = !value
    });
  };

  filterDateHandler(value, event, key) {
    const {stores: {problemSubmissionsUIStore}} = this.props;

    problemSubmissionsUIStore.runInAction(() => {
      problemSubmissionsUIStore.filter[key] = value
    });
  };

  render() {
    const {
      stores: {
        problemSubmissionsUIStore: {isLoading, filter, resetFilter},
        problemSubmissionsStore: {problemSubmissions}
      }
    } = this.props;
    const {submissionStatusId, shownFilterBodies} = this.state;

    return (
      <Container>
        <header className='p-index__heading'>
          <Title className='t-primary-headings'>Решения</Title>
        </header>
        <div className="p-index__body">
          <WithLoading loading={isLoading}>
            {problemSubmissions.length > 0
              ? problemSubmissions.map((x, i) => {
                const statusColor =
                  (x.Status === 'Mistake' && 'sw-color-red') ||
                  (x.Status === 'Correct' && 'sw-color-green') ||
                  (x.Status === 'Send' && 'sw-color-grey');

                return (
                  <Row key={x.Id}>
                    <Column col={12}>
                      <Card inner first={i === 0}>
                        <Card.PrimeSide>
                          <Card.Heading flexed>
                            <Link
                              to={
                                PROBLEM_SUBMISSION_ROUTE
                                  .replace(':problemId', x.ProblemId)
                                  .replace(':submissionId', x.Id)
                              }
                              className='sw-color-black'
                            >
                              <Title>
                                <p className='sw-title__code'>{x.ProblemCode}</p>
                                <p className='sw-title__slash'>|</p>
                                <p className='sw-title__text'>{x.ProblemName}</p>
                              </Title>
                            </Link>
                            <Card.Status className='sw-m-h-10'>
                              <div className={statusColor}>
                                <MaterialIcon
                                  className='sw-card-status__icon'
                                  name={
                                    (x.Status === 'Mistake' && 'error') ||
                                    (x.Status === 'Correct' && 'check_circle') ||
                                    (x.Status === 'Send' && 'send')
                                  }
                                />
                              </div>
                              <div
                                className={`sw-card-status__text ${statusColor}`}
                                onClick={() => this.toggleStatusDropbox(x.Id)}
                              >
                                {
                                  (x.Status === 'Mistake' && 'ОШИБКА') ||
                                  (x.Status === 'Correct' && 'ПРАВИЛЬНО') ||
                                  (x.Status === 'Send' && 'РЕШЕНИЕ ОТПРАВЛЕНО')
                                }
                              </div>
                            </Card.Status>
                            {submissionStatusId === x.Id &&
                            <Dropbox>
                              <span className='sw-m-t5-b10 sw-fw-bold'>Статус отправки</span>
                              <Divider/>
                              <span className='sw-m-t5-b10 sw-color-red'>{x.LogText}</span>
                              <span className='sw-m-t5-b10 sw-fw-bold'>Код</span>
                              <Divider/>
                              <span className='sw-m-t5-b10'>{x.SubmissionCode}</span>
                            </Dropbox>
                            }
                          </Card.Heading>
                          <Card.Body>
                            <SubTitle>{x.UserName}</SubTitle>
                          </Card.Body>
                        </Card.PrimeSide>
                        <Card.SecondarySide>
                          <Card.Heading>
                            <div className='sw-date'>
                              <div className='sw-date__divider'>
                                <div className="sw-date__icon">
                                  <MaterialIcon name='calendar_today'/>
                                </div>
                                <div className="sw-date__value">
                                  {moment(x.SubmitDateTime).format('DD.MM.YY')}
                                </div>
                              </div>
                              <div className='sw-date__divider'>
                                <div className="sw-date__icon">
                                  <MaterialIcon name='access_time'/>
                                </div>
                                <div className="sw-date__value">
                                  {moment(x.SubmitDateTime).format('H:m')}
                                </div>
                              </div>
                            </div>
                          </Card.Heading>
                          <Card.Body fixed>
                            <Card.Dropdown icon={<MaterialIcon name='more_horiz'/>}>
                              <Link
                                to={
                                  PROBLEM_SUBMISSION_ROUTE
                                    .replace(':problemId', x.ProblemId)
                                    .replace(':submissionId', x.Id)
                                }
                                className='sw-color-grey'
                              >
                                <Card.DropdownItem>
                                  Перейти к решению
                                </Card.DropdownItem>
                              </Link>
                            </Card.Dropdown>
                          </Card.Body>
                        </Card.SecondarySide>
                      </Card>
                    </Column>
                  </Row>
                )
              })
              : <p>Вы ещё не решили ни одной задачи</p>
            }
          </WithLoading>
        </div>
        <PortalComponent mountAt={this.rootFilterNode} container={this.filterContainer}>
          <FilterContainer>
            <FilterContainer.Header>
              <Icon color='white'>
                <MaterialIcon name='filter_list'/>
              </Icon>
              <FilterContainer.Title>
                Фильтр
              </FilterContainer.Title>
            </FilterContainer.Header>
            <FilterContainer.Body>
              <List>
                <List.Item>
                  <Filter>
                    <Filter.Header onClick={() => this.toggleFilterBody('status')}>
                      <List.Label className='sw-cursor-pointer' padding>
                        Статус задач
                      </List.Label>
                      <Icon color="white">
                        <MaterialIcon name="keyboard_arrow_down"/>
                      </Icon>
                    </Filter.Header>
                    {shownFilterBodies.includes('status') &&
                    <Filter.Body>
                      <CustomInput
                        className="sw-fz-13 sw-cursor-pointer sw-m-b-15"
                        id='filter-correct'
                        type='checkbox'
                        label="Правильно"
                        iconColor='white'
                        checked={filter.correct}
                        onChange={() => this.filterStatusHandler('correct', filter.correct)}
                      />
                      <CustomInput
                        className="sw-fz-13 sw-cursor-pointer sw-m-b-15"
                        id='filter-mistake'
                        type='checkbox'
                        label="Ошибка"
                        iconColor='white'
                        checked={filter.mistake}
                        onChange={() => this.filterStatusHandler('mistake', filter.mistake)}
                      />
                      <CustomInput
                        className="sw-fz-13 sw-cursor-pointer sw-m-b-15"
                        id='filter-send'
                        type='checkbox'
                        label='Отправлено'
                        iconColor='white'
                        checked={filter.send}
                        onChange={() => this.filterStatusHandler('send', filter.send)}
                      />
                    </Filter.Body>
                    }
                  </Filter>
                </List.Item>
                <List.Item>
                  <Filter>
                    <Filter.Header onClick={() => this.toggleFilterBody('date')}>
                      <List.Label className='sw-cursor-pointer' padding>
                        Дата отправки
                      </List.Label>
                      <Icon color="white">
                        <MaterialIcon name="keyboard_arrow_down"/>
                      </Icon>
                    </Filter.Header>
                    {shownFilterBodies.includes('date') &&
                    <Filter.Body className='sw-p-l-35'>
                      <List flexed>
                        <Form.Group className='sw-m-r-5 sw-m-b-0'>
                          <List.Item className='sw-flex-nowrap'>
                            <span className="sw-m-r-5 sw-fz-13">c</span>
                            <DatePicker
                              className="filter__date-input"
                              fixedHeight
                              selectsStart
                              dateFormat="dd/MM/yyyy"
                              minDate={new Date(2018, 0, 1)}
                              maxDate={new Date(2999, 11, 31)}
                              startDate={filter.fromDate}
                              endDate={filter.toDate}
                              onChange={(value, event) => this.filterDateHandler(value, event, 'fromDate')}
                              selected={filter.fromDate}
                              popperModifiers={{
                                offset: {
                                  enabled: true,
                                  offset: '-55px, -5px'
                                }
                              }}
                            />
                          </List.Item>
                        </Form.Group>
                        <Form.Group>
                          <List.Item className='sw-flex-nowrap'>
                            <span className="sw-m-r-5 sw-fz-13">по</span>
                            <DatePicker
                              className="filter__date-input"
                              fixedHeight
                              selectsEnd
                              dateFormat="dd/MM/yyyy"
                              minDate={new Date(2018, 0, 1)}
                              maxDate={new Date(2999, 11, 31)}
                              startDate={filter.fromDate}
                              endDate={filter.toDate}
                              onChange={(value, event) => this.filterDateHandler(value, event, 'toDate')}
                              selected={filter.toDate}
                              popperModifiers={{
                                offset: {
                                  enabled: true,
                                  offset: '0px, -5px'
                                }
                              }}
                            />
                          </List.Item>
                        </Form.Group>
                      </List>
                    </Filter.Body>
                    }
                  </Filter>
                </List.Item>
              </List>
            </FilterContainer.Body>
            <Filter.Footer>
              <Button color='violet' fluid icon onClick={this.getProblemSubmissions}>
                <MaterialIcon name='filter_list'/>
              </Button>
              <Button color='violet' fluid icon onClick={resetFilter}>
                <MaterialIcon name='clear'/>
              </Button>
            </Filter.Footer>
          </FilterContainer>
        </PortalComponent>
      </Container>
    );
  }
}

const applyWrappers = pipe(observer, inject('stores'));
export default applyWrappers(ProblemSubmissions);
