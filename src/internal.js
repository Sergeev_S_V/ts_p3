import * as LocalStorage from './core/local-storage';
import * as config from './core/config';
import * as utils from './core/utils';
import * as KEYS from "./core/keys";
import {API_V1 as apiV1} from './core/request';

// import order of stores IMPORTANT
import BaseStore from './stores/common/Base.store';
import ServiceStore from './stores/common/Service.store';
import BaseUIStore from './stores/common/BaseUI.store';
// models
// import order of models IMPORTANT
import SubjectModel from "./stores/models/Subject.model";
import SubjectTaskGroupModel from "./stores/models/SubjectTaskGroup.model";
import SubjectTaskModel from "./stores/models/SubjectTask.model";
import TaskProblemUserModel from "./stores/models/TaskProblemUser.model";
import ProblemModel from './stores/models/Problem.model';
import UserRoleModel from './stores/models/UserRole.model';
import UserModel from './stores/models/User.model';
import ProblemsFilterModel from './stores/models/ProblemsFilter.model';
import ProblemSubmissionsFilterModel from './stores/models/ProblemSubmissionsFilter.model';
import ProblemSubmissionModel from './stores/models/ProblemSubmission.model';
import TagModel from './stores/models/Tag.model';
import ProblemForTagLookUpModel from './stores/models/ProblemForTagLookUp.model';
import LinkForTagLookUpModel from './stores/models/LinkForTagLookUp.model';
// domain stores
import AuthStore from './stores/domain/Auth.store';
import ProblemStore from './stores/domain/Problem.store';
import ProblemSubmissionsStore from './stores/domain/ProblemSubmissions.store';
import TagsStore from './stores/domain/Tags.store';
// UI stores
import AppUIStore from "./stores/ui/App.ui.store";
import SigninUIStore from "./stores/ui/Signin.ui.store";
import SignupUIStore from "./stores/ui/Signup.ui.store";
import ProblemsUIStore from "./stores/ui/Problems.ui.store";
import ProblemSubmissionsUIStore from "./stores/ui/ProblemSubmissions.ui.store";
import ControlProblemsUIStore from "./stores/ui/ControlProblems.ui.store";
import TagsUIStore from './stores/ui/Tags.ui.store';

import RootStore from './stores/common/Root.store';


export * from "./routes";
export * from "./core/constants";

export {
  LocalStorage,
  config,
  utils,
  apiV1,
  KEYS,

  // common stores
  BaseStore,
  ServiceStore,
  AuthStore,
  RootStore,

  // domain stores
  ProblemStore,
  ProblemSubmissionsStore,
  TagsStore,

  // UI stores
  ProblemsUIStore,
  BaseUIStore,
  AppUIStore,
  SigninUIStore,
  SignupUIStore,
  ProblemSubmissionsUIStore,
  ControlProblemsUIStore,
  TagsUIStore,

  // models
  ProblemsFilterModel,
  UserRoleModel,
  UserModel,
  ProblemModel,
  TaskProblemUserModel,
  SubjectTaskModel,
  SubjectTaskGroupModel,
  SubjectModel,
  ProblemSubmissionModel,
  ProblemSubmissionsFilterModel,
  TagModel,
  ProblemForTagLookUpModel,
  LinkForTagLookUpModel
};
