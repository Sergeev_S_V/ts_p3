import {types, getEnv} from "mobx-state-tree";

import {BaseUIStore, UserRoleModel} from "../../internal";


const AppUIStore = BaseUIStore.named(
  "AppUIStore"
).props({
  currentRole: types.optional(UserRoleModel, {})
}).actions(self => {
  const KEYS = getEnv(self).KEYS;
  const LocalStorage = getEnv(self).LocalStorage;

  function afterCreate() {
    // create item to persist
    LocalStorage.getItem(KEYS.LS_KEY_PERSIST_APP_STORE).then((value) => {
      if (value === null) {
        LocalStorage.setItem(
          KEYS.LS_KEY_PERSIST_APP_STORE,
          JSON.stringify({currentRole: {}})
        );
      }
    });
  }

  function setRole(role) {
    self.currentRole = role;
  }

  return {
    setRole,
    afterCreate
  };
});

export default AppUIStore;
