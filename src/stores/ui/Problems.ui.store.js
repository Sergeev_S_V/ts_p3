import {types} from 'mobx-state-tree';

import {BaseUIStore, ProblemsFilterModel} from "../../internal";


const ProblemsUIStore = BaseUIStore.named(
  "ProblemsUIStore"
).props({
  filter: types.optional(ProblemsFilterModel, {})
});

export default ProblemsUIStore;
