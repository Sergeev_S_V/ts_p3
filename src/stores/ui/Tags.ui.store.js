import {BaseUIStore} from '../../internal';

const TagsUIStore = BaseUIStore.named('TagsUIStore');

export default TagsUIStore;
