import {types, applySnapshot, getSnapshot} from "mobx-state-tree";

import {BaseUIStore, ProblemSubmissionsFilterModel, LocalStorage, KEYS} from "../../internal";


const ProblemSubmissionsUIStore = BaseUIStore
.named('ProblemSubmissionsUIStore')
.props({
  filter: types.optional(ProblemSubmissionsFilterModel, {})
})
.actions(self => ({
  afterCreate() {
    LocalStorage.getItem(KEYS.LS_KEY_PERSIST_PROBLEM_SUBMISSIONS_UI_STORE)
    .then((value) => {
      if (value === null) {
        LocalStorage.setItem(
          KEYS.LS_KEY_PERSIST_PROBLEM_SUBMISSIONS_UI_STORE,
          JSON.stringify({filter: getSnapshot(self.filter)})
        );
      }
    });
  },
  resetFilter() {
    applySnapshot(
      self.filter,
      getSnapshot(ProblemSubmissionsFilterModel.create())
    );
  }
}));

export default ProblemSubmissionsUIStore;
