import {types} from "mobx-state-tree";

import {BaseStore} from "../../internal";


const ServiceStore = BaseStore.named(
  "ServiceStore"
).props({
  errors: types.map(
    types.union(
      types.array(types.string),
      types.string
    )
  )
});

export default ServiceStore;
