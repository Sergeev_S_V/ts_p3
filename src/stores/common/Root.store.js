import {types} from "mobx-state-tree";

import {
  AuthStore,
  AppUIStore,
  SigninUIStore,
  SignupUIStore,
  ProblemStore,
  ProblemsUIStore,
  ProblemSubmissionsStore,
  ProblemSubmissionsUIStore,
  ControlProblemsUIStore,
  TagsStore,
  TagsUIStore
} from '../../internal';


const RootStore = types.model(
  'RootStore',
  // injected stores
  {
    // domain stores
    authStore: AuthStore,
    problemStore: ProblemStore,
    problemSubmissionsStore: ProblemSubmissionsStore,
    tagsStore: TagsStore,
    tagsUIStore: TagsUIStore,
    // UI stores
    appStore: AppUIStore,
    signinStore: SigninUIStore,
    signupStore: SignupUIStore,
    problemsUIStore: ProblemsUIStore,
    problemSubmissionsUIStore: ProblemSubmissionsUIStore,
    // controlProblemUIStore: ControlProblemsUIStore
  }
);

export default RootStore;
