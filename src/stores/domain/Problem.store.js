import { getEnv, types } from "mobx-state-tree";
import { fromPromise, FULFILLED } from "mobx-utils";
import { when } from "mobx";

import {ServiceStore, ProblemModel} from "../../internal";


const ProblemStore = ServiceStore.named("ProblemStore")
.props({
  problems: types.optional(types.array(ProblemModel), [])
}).actions(self => {
  const apiV1 = getEnv(self).apiV1;

  function getUrl(url) {
    return '/api/ProblemsWebApi' + url;
  }

  const getProblems = filter => {
    const prom = fromPromise(apiV1.post(getUrl("/GetTaskProblems"), filter));
    when(
      () => prom.state === FULFILLED,
      () => {
        self.runInAction(() => {
          self.problems = prom.value.data;
        })
      }
    );

    return prom;
  };

  return {
    getProblems
  }
});

export default ProblemStore;
