import {types, getEnv} from "mobx-state-tree";
import {fromPromise, REJECTED, FULFILLED, PENDING} from "mobx-utils";
import {when} from "mobx";

import {ServiceStore, UserModel} from "../../internal";


const AuthStore = ServiceStore.named(
  "AuthStore"
).props({
  currentUser: types.optional(UserModel, {})
}).actions(self => {
  const apiV1 = getEnv(self).apiV1;
  const KEYS = getEnv(self).KEYS;
  const LocalStorage = getEnv(self).LocalStorage;

  function getUrl(url) {
    return "/api/AccountApi" + url;
  }

  const afterCreate = () => {
    // create item to persist
    LocalStorage.getItem(KEYS.LS_KEY_PERSIST_AUTH_STORE).then((value) => {
      if (value === null) {
        LocalStorage.setItem(
          KEYS.LS_KEY_PERSIST_AUTH_STORE,
          JSON.stringify({currentUser: {}})
        );
      }
    });
  };

  const getInfo = () => {
    const prom = fromPromise(apiV1.get(getUrl("/GetCurrentUserInfo")));

    when(
      () => prom.state === FULFILLED,
      () => {
        prom.case({
          fulfilled: ({data}) => {
            self.currentUser.setInfo(data);
          }
        });
      }
    );

    return prom;
  };

  const signIn = data => {
    const prom = fromPromise(apiV1.post("/Token", data));

    when(
      () => prom.state === PENDING,
      () => {
        prom.case({
          pending: () => {
            self.runInAction(() => {
              self.errors.delete(KEYS.SIGNIN_ERROR);
            });
          }
        });
      }
    );
    when(
      () => prom.state === REJECTED,
      () => {
        prom.case({
          rejected: ({response}) => {
            if (response) {
              self.runInAction(() => {
                self.errors.set(KEYS.SIGNIN_ERROR, response.data.error_description);
              });
            }
          }
        });
      }
    );
    when(
      () => prom.state === FULFILLED,
      () => {
        prom.case({
          fulfilled: ({data}) => {
            self.currentUser.setData(data);
          }
        });
      }
    );

    return prom;
  };

  const signOut = () => {
    self.currentUser = UserModel.create({});
  };

  const signUp = data => {
    const prom = fromPromise(apiV1.post(getUrl("/Register"), data));

    when(
      () => prom.state === PENDING,
      () => {
        prom.case({
          pending: () => {
            self.runInAction(() => {
              self.errors.delete(KEYS.SIGNUP_ERROR);
              self.errors.delete(KEYS.EMAIL_FIELD_ERROR);
              self.errors.delete(KEYS.PASSWORD_FIELD_ERROR);
              self.errors.delete(KEYS.CONFIRM_PASSWORD_FIELD_ERROR);
            });
          }
        });
      }
    );
    when(
      () => prom.state === REJECTED,
      () => {
        prom.case({
          rejected: ({response}) => {
            if (response && response.data && response.data.ModelState) {
              const {data: {ModelState}} = response;
              self.runInAction(() => {
                self.errors.set(
                  KEYS.SIGNUP_ERROR,
                  [].concat(ModelState[""] ? ModelState[""] : [])
                );
                self.errors.set(
                  KEYS.EMAIL_FIELD_ERROR,
                  [].concat(ModelState["model.Email"] ? ModelState["model.Email"] : [])
                );
                self.errors.set(
                  KEYS.PASSWORD_FIELD_ERROR,
                  [].concat(ModelState["model.Password"] ? ModelState["model.Password"] : [])
                );
                self.errors.set(
                  KEYS.CONFIRM_PASSWORD_FIELD_ERROR,
                  [].concat(ModelState["model.ConfirmPassword"] ? ModelState["model.ConfirmPassword"] : [])
                );
              });
            }
          }
        });
      }
    );

    return prom;
  };

  return {
    signIn,
    signOut,
    signUp,
    getInfo,
    afterCreate
  };
});

export default AuthStore;
