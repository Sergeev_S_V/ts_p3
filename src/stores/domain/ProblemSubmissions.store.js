import {types, getEnv} from 'mobx-state-tree';
import {fromPromise, FULFILLED} from "mobx-utils";
import { when } from "mobx";

import {ProblemSubmissionModel, ServiceStore} from "../../internal";


const ProblemSubmissionsStore = ServiceStore.named('ProblemSubmissionsStore')
.props({
  problemSubmissions: types.optional(types.array(ProblemSubmissionModel), [])
})
.actions(self => {
  const apiV1 = getEnv(self).apiV1;

  const getProblemSubmissions = filter => {
    const prom = fromPromise(apiV1.get(`/api/TaskSubmissionsListApi`, {params: filter}));

    when(
      () => prom.state === FULFILLED,
      () => {
        self.runInAction(() => {
          self.problemSubmissions = prom.value.data.data.map(x => (
            {
              ...x,
              SubmitDateTime: new Date(x.SubmitDateTime),
              CheckDateTime: new Date(x.CheckDateTime)
            }
          ));
        })
      }
    );

    return prom;
  };

  return {
    getProblemSubmissions
  }
});

export default ProblemSubmissionsStore;
