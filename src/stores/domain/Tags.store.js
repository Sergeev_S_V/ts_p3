import {when} from "mobx";
import {types, getEnv} from 'mobx-state-tree';
import {fromPromise, FULFILLED} from "mobx-utils";

import {
  TagModel, ProblemForTagLookUpModel, LinkForTagLookUpModel, ServiceStore
} from '../../internal';


const TagsStore = ServiceStore.named('TagsStore')
.props({
  tags: types.optional(types.array(TagModel), []),
  problemsForTagLookUp: types.optional(types.array(ProblemForTagLookUpModel), []),
  linksForTagLookUp: types.optional(types.array(LinkForTagLookUpModel), [])
})
.actions(self => {
  const apiV1 = getEnv(self).apiV1;
  const getUrl = () => 'api/TaskTagsApi';

  const getTags = () => {
    const prom = fromPromise(apiV1.get(getUrl()));

    when(
      () => prom.state === FULFILLED,
      () => {
        self.runInAction(() => {
          self.tags = prom.value.data.data;
        })
      }
    );

    return prom;
  };

  const addTag = data => (
    fromPromise(apiV1.post(getUrl(), data))
  );

  const editTag = data => (
    fromPromise(apiV1.post(getUrl(), data))
  );

  const deleteTag = id => (
    fromPromise(apiV1.delete(
      getUrl(),
      {
        headers: {'Content-type': 'application/json'},
        data: id,
      },
    ))
  );

  const getProblemsForTagLookUp = () => { // сделать в problems store
    const prom = fromPromise(apiV1.get('api/TaskProblemForTagLookUp'));

    when(
      () => prom.state === FULFILLED,
      () => {
        self.runInAction(() => {
          self.problemsForTagLookUp = prom.value.data.data;
        });
      }
    );

    return prom;
  };

  const getLinksForTagLookUp = () => { // сделать в links store
    const prom = fromPromise(apiV1.get('api/LinksLookUp'));

    when(
      () => prom.state === FULFILLED,
      () => {
        self.runInAction(() => {
          self.linksForTagLookUp = prom.value.data.data;
        });
      }
    );

    return prom;
  };

  return {
    getTags,
    addTag,
    editTag,
    deleteTag,
    getProblemsForTagLookUp,
    getLinksForTagLookUp
  }
});


export default TagsStore;
