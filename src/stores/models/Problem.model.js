import {types} from 'mobx-state-tree';

import {SubjectModel, SubjectTaskModel, TaskProblemUserModel} from '../../internal';

const Problem = types.model('Problem', {
  Id: types.maybeNull(types.identifierNumber),
  IsActive: types.maybeNull(types.boolean),
  IsFlagged: types.optional(types.boolean, false),
  IsSolved: types.optional(types.boolean, false),
  PercentOfUsersSolved: types.optional(types.number, 0),
  ProblemCode: types.optional(types.string, ''),
  ProblemLevel: types.union(
    types.literal(1), types.literal(2), types.literal(3), types.literal(4), types.literal(5)
  ),
  ProblemName: types.optional(types.string, ''),
  ProblemText: types.optional(types.string, ''),
  Raiting: types.optional(types.number, 0),
  Subject: SubjectModel,
  SubjectId: types.maybeNull(types.number),
  SubjectName: types.optional(types.string, ''),
  SubjectTasksList: types.array(SubjectTaskModel),
  Tags: types.optional(types.array(types.number), []),
  TaskCollections: types.optional(types.array(types.number), []),
  TaskProblemsUsersBusinessModel: types.array(TaskProblemUserModel),
  UserId: types.maybeNull(types.string)
});

export default Problem;
