import {types} from "mobx-state-tree";

import {SubjectTaskGroupModel} from '../../internal';

const SubjectTaskModel = types.model('SubjectTaskModel', {
  Disabled: types.optional(types.boolean, false),
  Group: SubjectTaskGroupModel,
  Selected: types.optional(types.boolean, false),
  Text: types.optional(types.string, ''),
  Value: types.optional(types.string, '')
});

export default SubjectTaskModel;
