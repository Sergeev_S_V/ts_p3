import {types} from 'mobx-state-tree';

const TagModel = types.model('TagModel', {
  Id: types.identifierNumber,
  TagName: types.optional(types.string, ''),
  Description: types.optional(types.string, ''),
  Links: types.optional(types.array(types.number), []),
  Problems: types.optional(types.array(types.number), [])
});

export default TagModel;
