import {types} from "mobx-state-tree";

const SubjectModel = types.model('SubjectModel', {
  Id: types.maybeNull(types.identifierNumber),
  Comment: types.maybeNull(types.string),
  CountProblems: types.optional(types.number, 0),
  Description: types.optional(types.string, ''),
  Diagram: types.maybeNull(types.string),
  SchemaCode: types.optional(types.string, ''),
  SelectedServers: types.optional(types.array(types.number), []),
  SubjectName: types.optional(types.string, '')
});

export default SubjectModel;
