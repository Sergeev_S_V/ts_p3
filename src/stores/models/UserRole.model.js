import {types} from "mobx-state-tree";


const UserRoleModel = types.model("UserRoleModel", {
  Role: "",
  LocalizedName: ""
});

export default UserRoleModel;
