import {types} from "mobx-state-tree";

const TaskProblemUserModel = types.model('TaskProblemUserModel', {
  Id: types.maybeNull(types.identifierNumber),
  UserId: types.optional(types.string, ''),
  Comment: types.maybeNull(types.string),
  IsFlagged: types.optional(types.boolean, false),
  ProblemId: types.maybeNull(types.number),
  Raiting: types.maybeNull(types.number),
  SavedCode: types.maybeNull(types.string)
});

export default TaskProblemUserModel;
