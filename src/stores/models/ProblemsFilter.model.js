import {types} from "mobx-state-tree";

const ProblemsFilterModel = types.model('ProblemsFilterModel', {
  page: types.optional(types.number, 1),
  subjectNames: types.optional(types.array(types.string), []),
  collections: types.optional(types.array(types.string), []),
  tags: types.optional(types.array(types.string), []),
  level: types.optional(types.array(types.number), []),
  solved: types.optional(types.boolean, false),
  notSolved: types.optional(types.boolean, false)
});

export default ProblemsFilterModel;
