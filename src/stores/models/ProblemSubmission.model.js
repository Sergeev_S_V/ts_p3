import {types} from 'mobx-state-tree';

const ProblemSubmissionModel = types.model('ProblemSubmissionModel', {
  Id: types.identifierNumber,
  ProblemId: types.maybeNull(types.number),
  ProblemName: types.optional(types.string, ''),
  ProblemCode: types.optional(types.string, ''),
  UserId: types.maybeNull(types.string),
  Status: types.optional(types.string, ''),
  SubmitDateTime: types.maybeNull(types.Date),
  CheckDateTime: types.maybeNull(types.Date),
  IsAccepted: types.optional(types.boolean, false),
  LogText: types.maybeNull(types.string),
  SubmitedQuery: types.maybeNull(types.string),
  UserName: types.maybeNull(types.string),
  GroupName: types.maybeNull(types.string),
  SubmissionCode: types.optional(types.string, '')
});

export default ProblemSubmissionModel;
