import {types} from "mobx-state-tree";

import {UserRoleModel} from "../../internal";


const UserModel = types.model("UserModel", {
  UserId: types.optional(types.identifier, ""),
  Roles: types.optional(types.array(UserRoleModel), []),
  expires: types.maybeNull(types.Date),
  issued: types.maybeNull(types.Date),
  access_token: "",
  expires_in: 0,
  token_type: "",
  userName: ""
}).actions(self => {

  const setData = (userData) => {
    self.expires = new Date(userData[".expires"] || userData.expires);
    self.issued = new Date(userData[".issued"] || userData.issued);
    self.access_token = userData.access_token;
    self.expires_in = userData.expires_in;
    self.token_type = userData.token_type;
    self.userName = userData.userName;
  };

  const setInfo = (userInfo) => {
    self.Roles = userInfo.Roles;
  };

  return {
    setData,
    setInfo
  };
});

export default UserModel;



