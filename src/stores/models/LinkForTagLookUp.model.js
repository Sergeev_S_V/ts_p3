import {types} from 'mobx-state-tree';


const LinkForTagLookUpModel = types.model('LinkForTagLookUpModel', {
  LinkId: types.identifierNumber,
  LinkName: types.optional(types.string, ''),
  LinkType: types.optional(types.string, ''),
  IsApproved: types.optional(types.boolean, false)
});

export default LinkForTagLookUpModel;
