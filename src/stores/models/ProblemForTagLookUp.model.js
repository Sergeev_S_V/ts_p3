import {types} from 'mobx-state-tree';

const ProblemForTagLookUpModel = types.model('ProblemForTagLookUpModel', {
  ProblemId: types.identifierNumber,
  ProblmName: types.optional(types.string, ''),
  SubjectName: types.optional(types.string, '')
});

export default ProblemForTagLookUpModel;
