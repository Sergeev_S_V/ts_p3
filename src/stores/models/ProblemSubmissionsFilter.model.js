import {types} from 'mobx-state-tree';

const ProblemSubmissionsFilterModel = types.model('ProblemSubmissionsFilterModel', {
  send: types.optional(types.boolean, false),
  correct: types.optional(types.boolean, false),
  mistake: types.optional(types.boolean, false),
  fromDate: types.maybeNull(types.Date),
  toDate: types.maybeNull(types.Date)
});

export default ProblemSubmissionsFilterModel;
