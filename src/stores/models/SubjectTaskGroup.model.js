import {types} from "mobx-state-tree";

const SubjectTaskGroup = types.model('SubjectTaskGroup', {
  Disabled: types.optional(types.boolean, false),
  Name: types.optional(types.string, '')
});

export default SubjectTaskGroup;
