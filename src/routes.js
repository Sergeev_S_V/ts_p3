import React from "react";
import loadable from "@loadable/component";

import {ErrorBoundary} from "./components";
import {
  AUTH_STUDENT_ROLE,
  AUTH_TEACHER_ROLE,
  AUTH_AUTHOR_ROLE,
  AUTH_ADMIN_ROLE
} from "./internal";


const Loading = () => (<div>Loading...</div>);

const DefaultLayout = loadable(() => import('./containers/DefaultLayout/'), {
  fallback: (<Loading/>),
});
const Signin = loadable(() => import('./views/pages/Signin/'), {
  fallback: (<Loading/>),
});
const Signup = loadable(() => import('./views/pages/Signup/'), {
  fallback: (<Loading/>),
});
const Problems = loadable(() => import('./views/Problems/'), {
  fallback: (<Loading/>),
});
const ControlProblems = loadable(() => import('./views/control/Problems'), {
  fallback: (<Loading/>),
});
const ProblemSubmissions = loadable(() => import('./views/ProblemSubmissions'), {
  fallback: (<Loading/>),
});
const Tags = loadable(() => import('./views/Tags'), {
  fallback: (<Loading/>)
});
const Page404 = loadable(() => import('./views/pages/404/'), {
  fallback: (<Loading/>),
});
const Page500 = loadable(() => import('./views/pages/500/'), {
  fallback: (<Loading/>),
});

export const ROOT_ROUTE = '/';
export const SIGN_IN_ROUTE = '/signin';
export const SIGN_UP_ROUTE = '/signup';
export const PROBLEM_SUBMISSION_ROUTE = '/problem/:problemId/submission/:submissionId';
export const PROBLEM_SUBMISSIONS_ROUTE = '/problem/submissions';
export const PROBLEMS_ROUTE = '/problems';
export const TAGS_ROUTE = '/tags';
export const CONTROL_PROBLEMS_ROUTE = "/control/problems";
export const NOT_FOUND_ROUTE = '/404';
export const INTERNAL_ERROR_ROUTE = '/500';

export const TYPE_ROUTE = 'route';
export const TYPE_REDIRECT = 'redirect';

export const ROUTE_LEVEL_I = "route-level-l";
export const ROUTE_LEVEL_II = "route-level-ll";

const ALL_AUTH_ROLES = [
  AUTH_ADMIN_ROLE,
  AUTH_AUTHOR_ROLE,
  AUTH_TEACHER_ROLE,
  AUTH_STUDENT_ROLE,
];

export const routes = [
  // level 1 routes
  {
    key: "signin-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_I,
    path: SIGN_IN_ROUTE,
    exact: true,
    name: 'Sign in',
    render: (props) => (
      <ErrorBoundary>
        <Signin {...props} />
      </ErrorBoundary>
    )
  },
  {
    key: "signup-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_I,
    path: SIGN_UP_ROUTE,
    exact: true,
    name: 'Sign up',
    render: (props) => (
      <ErrorBoundary>
        <Signup {...props} />
      </ErrorBoundary>
    )
  },
  {
    key: "404-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_I,
    path: NOT_FOUND_ROUTE,
    exact: true,
    name: 'Page 404',
    render: (props) => (
      <ErrorBoundary>
        <Page404 {...props} />
      </ErrorBoundary>
    )
  },
  {
    key: "500-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_I,
    path: INTERNAL_ERROR_ROUTE,
    exact: true,
    name: 'Page 500',
    render: (props) => (
      <ErrorBoundary>
        <Page500 {...props} />
      </ErrorBoundary>
    )
  },
  // route "/" must be at the end of level 1 routes
  {
    key: "main-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_I,
    path: ROOT_ROUTE,
    exact: false,
    name: 'Main',
    render: (props) => (
      <ErrorBoundary>
        <DefaultLayout {...props} />
      </ErrorBoundary>
    )
  },
  // redirects of level I routes
  {
    key: "redirect-to-main",
    type: TYPE_REDIRECT,
    level: ROUTE_LEVEL_I,
    to: NOT_FOUND_ROUTE
  },
  // level II routes
  {
    key: "problems-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_II,
    roles: [''],
    path: PROBLEMS_ROUTE,
    exact: true,
    name: 'Problems page',
    render: (props) => (
      <ErrorBoundary>
        <Problems {...props} />
      </ErrorBoundary>
    )
  },
  {
    key: "problem-submissions-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_II,
    roles: [''],
    path: PROBLEM_SUBMISSIONS_ROUTE,
    exact: true,
    name: 'Problem submissions page',
    render: (props) => (
      <ErrorBoundary>
        <ProblemSubmissions {...props} />
      </ErrorBoundary>
    )
  },
  {
    key: "tags-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_II,
    roles: [''],
    path: TAGS_ROUTE,
    exact: true,
    name: 'Tags page',
    render: (props) => (
      <ErrorBoundary>
        <Tags {...props} />
      </ErrorBoundary>
    )
  },
  {
    key: "control-problems-page",
    type: TYPE_ROUTE,
    level: ROUTE_LEVEL_II,
    roles: [''],
    path: CONTROL_PROBLEMS_ROUTE,
    exact: true,
    name: 'Control problems page',
    render: (props) => (
      <ErrorBoundary>
        <ControlProblems {...props} />
      </ErrorBoundary>
    )
  },
  // redirects of level II routes
  {
    key: "redirect-from-root-to-problems",
    type: TYPE_REDIRECT,
    level: ROUTE_LEVEL_II,
    from: ROOT_ROUTE,
    to: PROBLEMS_ROUTE
  },
];
