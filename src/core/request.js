import axios from 'axios';

import {config} from "../internal";

export const API_V1 = axios.create({
  baseURL: config.API_V1_URL
});
