export const SIGNIN_ERROR = "sw-key-signin-error";
export const SIGNUP_ERROR = "sw-key-signup-error";
export const EMAIL_FIELD_ERROR = "sw-key-email-field-error";
export const PASSWORD_FIELD_ERROR = "sw-key-password-field-error";
export const CONFIRM_PASSWORD_FIELD_ERROR = "sw-key-confirm-password-field-error";

export const LS_KEY_PERSIST_AUTH_STORE = "sw-ls-key-persist-auth-store";
export const LS_KEY_PERSIST_APP_STORE = "sw-ls-key-persist-app-store";
export const LS_KEY_PERSIST_PROBLEMS_UI_STORE = "sw-ls-key-persist-problems-ui-filter-store";
export const LS_KEY_PERSIST_PROBLEM_SUBMISSIONS_UI_STORE = "sw-ls-key-persist-problem-submissions-ui-filter-store";

