import PropTypes from "prop-types";
import {onSnapshot, applySnapshot} from 'mobx-state-tree';
import MobxReactForm from 'mobx-react-form';
import validatorjs from 'validatorjs';

import {LocalStorage} from "../internal";


/** Функция кодирования значений для query string */
export const encodeQueryString = data => {
  const dataKeys = Object.keys(data);
  const arrLength = dataKeys.length;
  return dataKeys.reduce((acc, dataKey, i) => {
    const isLastElementOfArray = i + 1 === arrLength;
    const URIComponent = encodeURIComponent(data[dataKey]);
    acc = acc + `${dataKey}=${URIComponent}${!isLastElementOfArray ? '&' : ''}`;
    return acc;
  }, '');
};

/**
 * Функция проверки если доступно хранилище
 *
 * @param: {string} type
 * @return: {boolean}
 * Если true, то хранилище доступно
 * Если false, то хранилище не доступно
 */
export function storageAvailable(type) {
  try {
    var storage = window[type],
      x = '__storage_test__';
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return (
      e instanceof DOMException &&
      // everything except Firefox
      (e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === "QuotaExceededError" ||
        // Firefox
        e.name === "NS_ERROR_DOM_QUOTA_REACHED") &&
      // acknowledge QuotaExceededError only if there's something already stored
      storage.length !== 0
    );
  }
}

/**
 * Функция для проверки параметра на целое число
 *
 * Пример:
 *  null => false
 *  NaN => false
 *  'string' => false
 *  true => false
 *
 * @param {any} num
 */
export function isNumber(num) {
  return !(isNaN(parseInt(num)) || !isFinite(num));
}

/**
 * Капитализация первого символа строки
 *
 * @param {str} str
 */
export function capitalize(str) {
  if (typeof str === "string" && !str) {
    return new Error("Invalid argument type must be a string.");
  }

  const _str = str.trim();
  return _str.charAt(0).toUpperCase() + _str.slice(1);
}

// Shim Element if needed (e.g. in Node environment)
const Element = typeof window === 'object' && (window.Element || function () {});

export function DOMElement(props, propName, componentName) {
  if (!(props[propName] instanceof Element)) {
    return new Error(
      'Invalid prop `' +
      propName +
      '` supplied to `' +
      componentName +
      '`. Expected prop to be an instance of Element. Validation failed.'
    );
  }
}

/**
 * Общая утилита если тип свойства Component
 */
export const tagPropType = PropTypes.oneOfType([
  PropTypes.func,
  PropTypes.string,
  PropTypes.node,
  PropTypes.shape({$$typeof: PropTypes.symbol, render: PropTypes.func}),
  PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.node,
      PropTypes.string,
      PropTypes.shape({$$typeof: PropTypes.symbol, render: PropTypes.func})
    ])
  )
]);

export const targetPropType = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.func,
  DOMElement,
  PropTypes.shape({current: PropTypes.any}),
]);

/**
 * Returns a new object with the key/value pairs from `obj` that are not in the array `omitKeys`.
 */
export function omit(obj, omitKeys) {
  const result = {};
  Object.keys(obj).forEach(key => {
    if (omitKeys.indexOf(key) === -1) {
      result[key] = obj[key];
    }
  });
  return result;
}

/**
 * Returns a filtered copy of an object with only the specified keys.
 */
export function pick(obj, keys) {
  const pickKeys = Array.isArray(keys) ? keys : [keys];
  let length = pickKeys.length;
  let key;
  const result = {};

  while (length > 0) {
    length -= 1;
    key = pickKeys[length];
    result[key] = obj[key];
  }
  return result;
}

/**
 * общая утилита чтобы удалить пробелы в начале и в конце строки,
 * и удалить двойные пробелы
 */
export const removeRedundantSpaces = str => {
  return str.trim().replace(/\s\s+/, " ");
};

/**
 * Функция генерации случайного целого числа, интервал [100000, 999999]
 *
 * @return {Number}
 * */
export function generateKey() {
  return Math.floor(Math.random() * 1000000);
}

/**
 * Общие цвета по умолчанию
 * */
export const defaultColors = {
  grey: "#7F8C8D",
  yellow: "#F5CE00",
  green: "#27AE60",
  red: "#E74C3C"
};

/**
 * Функция сохранения Store обьектов в хранилище
 *
 * @param {String} name Ключ сохранения в хранилище
 * @param {MST instance} store Обьект типа Store
 * @param {Object} options Обьект параметров.
 * Схема:
 * {
 *     storage: <Storage> тип хранилища (напр, localStorage)
 *     jsonify: <Boolean> если true - сериализовывать, иначе false
 * }
 * @param {Object} schema Обьект указывающий какие свойства сохранить
 * Схема:
 * {
 *     [propName]: <Boolean> если true - сохранить, иначе false
 * }
 * */
export const persist = (name, store, options, schema = {}) => {
  let hydrated = false;

  let storage = options.storage;

  if (typeof localStorage !== "undefined" && localStorage === storage) {
    storage = LocalStorage;
  }

  onSnapshot(store, _snapshot => {
    if (!hydrated) {
      return;
    }
    const snapshot = {..._snapshot};
    Object.keys(snapshot).forEach(key => {
      if (!schema[key]) {
        delete snapshot[key];
      }
    });
    const data = !options.jsonify ? snapshot : JSON.stringify(snapshot);
    storage.setItem(name, data);
  });

  storage.getItem(name).then(data => {
    if (data) {
      const snapshot = !options.jsonify ? data : JSON.parse(data);
      applySnapshot(store, snapshot);
      if (store.afterHydration && typeof store.afterHydration === "function") {
        store.afterHydration();
      }
      hydrated = true;
    }
  });
};


export const createForm = (firstArg, secondArg) => {

  secondArg.plugins = {dvr: validatorjs};

  return new MobxReactForm(firstArg, secondArg);
};
