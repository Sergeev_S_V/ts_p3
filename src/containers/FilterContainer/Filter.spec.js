import React from "react";
import {mount} from "enzyme";

import Filter from "./index";


describe("<FilterContainer/>", () => {

  const div = document.createElement('div');

  it("should render without crashing", () => {
    const wrapper = mount(
      <Filter>
        <Filter.Header>
          <Filter.Title>Title</Filter.Title>
        </Filter.Header>
        <Filter.Body>
          Body
        </Filter.Body>
        <Filter.Footer>
          Footer
        </Filter.Footer>
      </Filter>,
      {attachTo: div}
    );

    expect(wrapper.html()).toBe(
      '<div class="sw-filter">' +
      '<header class="sw-filter__heading">' +
      '<span class="sw-filter__title">Title</span>' +
      '</header>' +
      '<div class="sw-filter__body">Body</div>' +
      '<div class="sw-filter__footer">Footer</div>' +
      '</div>'
    );
  });

});

