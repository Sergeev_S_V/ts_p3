import React, {Component} from "react";
import {observer, inject} from "mobx-react";
import {withRouter, Route, Switch, Redirect} from "react-router-dom";
import {pipe} from "ramda";

import {
  Container,
  Row,
  List,
  Navigation,
  Header,
  Subject,
  Icon,
  MaterialIcon
} from "../../components";
import {TYPE_ROUTE, routes, ROUTE_LEVEL_II, TYPE_REDIRECT, config} from "../../internal";

import SQLIconPng from "../../assets/img/sql-logo.png";


class DefaultLayout extends Component {

  renderHeader() {
    return (
      <Header className="sw-col">
        <Header.Inner>
          <List>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="star_border"
                />
              </Icon>
            </List.Item>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="notifications_none"
                />
              </Icon>
            </List.Item>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="search"
                />
              </Icon>
            </List.Item>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="equalizer"
                />
              </Icon>
            </List.Item>
          </List>

          <List>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="list"
                />
              </Icon>
            </List.Item>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="people"
                />
              </Icon>
            </List.Item>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="person_add"
                />
              </Icon>
            </List.Item>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="arrow_forward"
                />
              </Icon>
            </List.Item>
          </List>

          <List>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="apps"
                />
              </Icon>
            </List.Item>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="help_outline"
                />
              </Icon>
            </List.Item>
            <List.Item>
              <Icon centered>
                <MaterialIcon
                  className="sw-color-white"
                  name="person"
                />
              </Icon>
            </List.Item>
          </List>
        </Header.Inner>
      </Header>
    );
  }

  renderNavigation() {
    return (
      <Navigation className="sw-col-3">
        <Navigation.Inner>
          <Navigation.Header>
            <figure className="logo">
              <img src={SQLIconPng} alt=""/>
            </figure>
            <Subject>
              <Subject.Title>
                Задачи по SQL
              </Subject.Title>
              <Subject.Title subtitle>
                Архив задач
              </Subject.Title>
            </Subject>
          </Navigation.Header>
          <Navigation.Body>
            <Navigation.Items>
              <List>
                <List.Item>
                  <Icon>
                    <MaterialIcon
                      className="sw-color-white"
                      name="list"
                    />
                  </Icon>
                  <List.Label>
                    Задачи
                  </List.Label>
                </List.Item>
                <List.Item>
                  <Icon>
                    <MaterialIcon
                      className="sw-color-white"
                      name="people"
                    />
                  </Icon>
                  <List.Label>
                    Группы
                  </List.Label>
                </List.Item>
                <List.Item>
                  <Icon>
                    <MaterialIcon
                      className="sw-color-white"
                      name="person_add"
                    />
                  </Icon>
                  <List.Label>
                    Запросы
                  </List.Label>
                </List.Item>
                <List.Item>
                  <Icon>
                    <MaterialIcon
                      className="sw-color-white"
                      name="arrow_forward"
                    />
                  </Icon>
                  <List.Label>
                    Решения
                  </List.Label>
                </List.Item>
              </List>
            </Navigation.Items>
            <Navigation.Items id={config.MOUNT_FILTER_ID}/>
          </Navigation.Body>
        </Navigation.Inner>
      </Navigation>
    );
  }

  render() {
    const {stores: {appStore: {currentRole}}} = this.props;

    return (
      <Container fluid>
        <Row className="sw-height-100">
          {this.renderHeader()}
          <main className="sw-col-fluid l-main">
            <Row className="sw-height-100">
              {this.renderNavigation()}
              <section className="sw-col-fluid l-main-content">
                <Row className="sw-height-100">
                  <section className="sw-col-fluid p-index">
                    <div className="p-index__inner">
                      <Switch>
                        {routes.map(route => {
                          if (route.level === ROUTE_LEVEL_II) {
                            if (route.type === TYPE_ROUTE) {
                              const {roles} = route;
                              return roles.includes(currentRole.Role) ? (<Route {...route}/>) : null;
                            } else if (route.type === TYPE_REDIRECT) {
                              return (<Redirect {...route}/>);
                            }
                          }
                          return null;
                        })}
                      </Switch>
                    </div>
                  </section>
                </Row>
              </section>
            </Row>
          </main>
        </Row>
      </Container>
    );
  }
}

const applyWrappers = pipe(
  observer,
  inject("stores"),
  withRouter
);

export default applyWrappers(DefaultLayout);
