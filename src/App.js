import React from "react";
import {BrowserRouter, Route, Switch, Redirect} from "react-router-dom";
import {observer, inject} from "mobx-react";
import {pipe} from "ramda";

import {TYPE_ROUTE, routes, ROUTE_LEVEL_I, TYPE_REDIRECT} from "./internal";


class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          {routes.map(route => {
            if (route.level === ROUTE_LEVEL_I) {
              if (route.type === TYPE_ROUTE) {
                return (<Route {...route}/>);
              } else if (route.type === TYPE_REDIRECT) {
                return (<Redirect {...route}/>);
              }
            }
            return null;
          })}
        </Switch>
      </BrowserRouter>
    );
  }
}

const applyWrappers = pipe(observer, inject("stores"));

export default applyWrappers(App);
