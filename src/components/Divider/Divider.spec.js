import React from "react";
import { shallow } from "enzyme";
import Divider from "./";

describe("<Divider />", () => {
  it("should render div element with class sw-divider", () => {
    const wrapper = shallow(<Divider />);

    expect(wrapper.html()).toBe('<div class="sw-divider"></div>');
  });
});
