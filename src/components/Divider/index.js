import React from "react";
import classNames from "classnames";
import t from "prop-types";

import { utils } from "../../internal";
import "./Divider.scss";

const Divider = ({ tag: Tag, className, ...attributes }) => {
  const classes = classNames("sw-divider", className);

  return <Tag className={classes} {...attributes} />;
};

Divider.propTypes = {
  tag: utils.tagPropType,
  className: t.string
};

Divider.defaultProps = {
  tag: "div"
};

export default Divider;
