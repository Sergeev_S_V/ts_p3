import React from "react";
import { shallow } from "enzyme";
import Container from "./";

describe("<Container />", () => {
  it("should render div element with class sw-container", () => {
    const wrapper = shallow(<Container />);

    expect(wrapper.html()).toBe('<div class="sw-container"></div>');
  });

  it("should render div element with class sw-container-fluid", () => {
    const wrapper = shallow(<Container fluid />);

    expect(wrapper.hasClass("sw-container-fluid")).toBe(true);
  });

  it("should render div element with child", () => {
    const wrapper = shallow(<Container>Children</Container>);

    expect(wrapper.html()).toBe('<div class="sw-container">Children</div>');
  });

  it("should render div element with nested class name", () => {
    const wrapper = shallow(<Container className="foo bar" />);

    expect(wrapper.hasClass("foo")).toBe(true);
    expect(wrapper.hasClass("bar")).toBe(true);
    expect(wrapper.hasClass("sw-container")).toBe(true);
  });

  it("should render div element with custom tag header", () => {
    const wrapper = shallow(
      <Container tag="header" fluid>
        ZZZ
      </Container>
    );

    expect(wrapper.text()).toBe("ZZZ");
    expect(wrapper.hasClass("sw-container-fluid")).toBe(true);
    expect(wrapper.type()).toBe("header");
  });
});
