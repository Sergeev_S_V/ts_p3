import React from "react";
import { shallow } from "enzyme";
import Input from "./";

describe("<Input />", () => {
  it("should render div element with class sw-input and sw-input--default", () => {
    const wrapper = shallow(
      <Input id="search" name="search" placeholder="Input some to search" />
    );

    expect(wrapper.html()).toBe(
      '<input type="text" class="sw-input sw-input--default" id="search" name="search" placeholder="Input some to search"/>'
    );
  });

  it("should render div element with class sw-input and sw-input--error", () => {
    const wrapper = shallow(
      <Input
        id="user_name"
        name="user_name"
        placeholder="Input your user name"
        color="error"
      />
    );

    expect(wrapper.hasClass("sw-input--error")).toBe(true);
  });

  it("should render div element with class sw-input and sw-input--success", () => {
    const wrapper = shallow(
      <Input
        id="user_name"
        name="user_name"
        placeholder="Input your user name"
        color="success"
      />
    );

    expect(wrapper.hasClass("sw-input--success")).toBe(true);
  });

  it("should render div element with class sw-input and sw-input--warning", () => {
    const wrapper = shallow(
      <Input
        id="user_name"
        name="user_name"
        placeholder="Input your user name"
        color="warning"
      />
    );

    expect(wrapper.hasClass("sw-input--warning")).toBe(true);
  });

  it("should render div element with class sw-input, sw-input--warning and sw-input--fluid", () => {
    const wrapper = shallow(
      <Input
        id="user_name"
        name="user_name"
        placeholder="Input your user name"
        fluid
      />
    );

    expect(wrapper.hasClass("sw-input")).toBe(true);
    expect(wrapper.hasClass("sw-input--fluid")).toBe(true);
  });
});
