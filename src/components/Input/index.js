import React, {Component} from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";

import "./Input.scss";


class Input extends Component {
  render() {
    const {tag: Tag, className, check, size, ...attributes} = this.props;

    const classes = classNames(
      "sw-input",
      size && `sw-input--${size}`,
      check && "sw-input__checkbox",
      Tag === "textarea" && "sw-input__textarea",
      className
    );

    return (<Tag className={classes} {...attributes} />);
  }
}

Input.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
  size: t.oneOf(["bg", "sm"]),
  check: t.bool
};

Input.defaultProps = {
  tag: "input"
};

export default Input;
