import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";


const CardBody = ({tag: Tag, fixed, className, ...attributes}) => {
  const classes = classNames(
    "sw-card__body",
    (fixed ? "sw-card__body--fixed" : null),
    className
  );

  return (<Tag className={classes} {...attributes}/>);
};

CardBody.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
  fixed: t.bool
};

CardBody.defaultProps = {
  tag: "div"
};

export default CardBody;
