import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";


class CardIcon extends React.Component {

  render() {
    const {
      tag: Tag,
      className,
      ...attributes
    } = this.props;
    const classes = classNames("sw-card-icon", className);

    return (
      <Tag className={classes} {...attributes}/>
    );
  }
}

CardIcon.propTypes = {
  tag: utils.tagPropType,
  className: t.string
};

CardIcon.defaultProps = {
  tag: "div"
};

export default CardIcon;
