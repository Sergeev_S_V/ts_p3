import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";


const CardPrimeSide = ({tag: Tag, className, ...attributes}) => {
  const classes = classNames("sw-card__prime-side", className);

  return (<Tag className={classes} {...attributes}/>);
};

CardPrimeSide.propTypes = {
  tag: utils.tagPropType,
  className: t.string
};

CardPrimeSide.defaultProps = {
  tag: "div"
};

export default CardPrimeSide;
