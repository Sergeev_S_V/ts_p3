import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";


class CardDropdown extends React.Component {

  state = {
    menuOpened: false
  };

  toggleMenu = () => {
    if (!this.props.disabled) {
      this.setState(prevState => ({
        menuOpened: !prevState.menuOpened
      }));
    }
  };

  render() {
    const {
      tag: Tag,
      innerTag: InnerTag,
      className,
      icon,
      children,
      ...attributes
    } = this.props;

    const classes = classNames(
      "sw-card-more-button",
      className
    );

    return (
      <Tag className={classes} {...attributes} onClick={this.toggleMenu}>
        {icon}
        {this.state.menuOpened && (
          <InnerTag className="sw-card-more-button__dropdown">
            {children}
          </InnerTag>
        )}
      </Tag>
    );
  }
}

CardDropdown.propTypes = {
  tag: utils.tagPropType,
  innerTag: utils.tagPropType,
  className: t.string,
  icon: utils.tagPropType.isRequired,
  disabled: t.bool,
  children: utils.tagPropType.isRequired,
};

CardDropdown.defaultProps = {
  tag: "div",
  innerTag: "div"
};

export default CardDropdown;

