import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";


const CardHeading = ({flexed, tag: Tag, className, ...attributes}) => {
  const classes = classNames(
    "sw-card__heading",
    (flexed ? "sw-card__heading--flexed" : null),
    className
  );

  return (<Tag className={classes} {...attributes}/>);
};

CardHeading.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
  flexed: t.bool
};

CardHeading.defaultProps = {
  tag: 'div'
};

export default CardHeading;
