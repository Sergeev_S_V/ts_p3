import React from "react";
import t from "prop-types";
import classNames from "classnames";
import {Manager} from 'react-popper';

import DropdownItems from "./DropdownItems";
import DropdownItem from "./DropdownItem";

import "./Dropdown.scss";

import {utils} from "../../internal";


class Dropdown extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
  }

  toggle(e) {
    if (this.props.disabled) {
      return e && e.preventDefault();
    }

    return this.props.toggle(e);
  }

  render() {
    const {
      tag: Tag,
      className,
      name,
      ...attributes
    } = utils.omit(this.props, []);

    const classes = classNames(
      "sw-dropdown",
      className
    );

    return (
      <Manager className={classes} {...attributes}/>
    );
  }

}

Dropdown.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
  name: t.string,
  disabled: t.bool,
  toggle: t.func,
  isOpen: t.bool,
  direction: t.oneOf(['up', 'down', 'left', 'right']),
};

Dropdown.defaultProps = {
  tag: "div",
  isOpen: false,
  disabled: false,
  direction: "down",
};

const childContextTypes = {
  toggle: t.func.isRequired,
  isOpen: t.bool.isRequired,
  direction: t.oneOf(['up', 'down', 'left', 'right']).isRequired
};

Dropdown.Items = DropdownItems;
Dropdown.Item = DropdownItem;
Dropdown.childContextTypes = childContextTypes;

export default Dropdown;
