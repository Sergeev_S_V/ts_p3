import React from "react";
import classNames from "classnames";
import t from "prop-types";

import {utils} from "../../internal";


const DropdownItem = ({tag: Tag, className, ...attributes}) => {
  const classes = classNames(
    "sw-dropdown-item",
    className
  );

  return (<Tag className={classes} {...attributes}/>);
};

DropdownItem.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
};

DropdownItem.defaultProps = {
  tag: "li"
};

export default DropdownItem;
