import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";


const DropdownItems = ({tag: Tag, className, ...attributes}) => {
  const classes = classNames(
    "sw-dropdown__items",
    className
  );

  return (<Tag className={classes} {...attributes}/>);
};

DropdownItems.propTypes = {
  tag: utils.tagPropType,
  className: t.string
};

DropdownItems.defaultProps = {
  tag: "ul",
};

export default DropdownItems;
