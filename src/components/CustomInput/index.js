import React from "react";
import t from "prop-types";
import classNames from "classnames";
import Select from 'react-select';

import {Icon, MaterialIcon} from "../index";

import "./CustomInput.scss";


const selectCustomStyles = {
  control: (provided, state) => ({
    ...provided,
    border: state.isSelected ? '1px solid #470075' : 'none'
  }),
  valueContainer: (provided) => ({
    ...provided,
    padding: '0 5px'
  }),
  container: (provided) => ({
    ...provided,
    width: '100%'
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    backgroundColor: 'none'
  })
};

class CustomInput extends React.Component {

  render() {
    const {type, label, className, iconColor, children, ...attributes} = this.props;

    const wrapperClass = classNames(
      className,
      `sw-custom-${type}`
    );

    if (type === "checkbox") {
      return (
        <label className={classNames(
          wrapperClass,
          attributes.disabled && "sw-custom-checkbox__disabled"
        )}>{label}
          <input
            type={type}
            {...attributes}
          />
          <Icon color={iconColor}>
            <MaterialIcon name="check"/>
          </Icon>
          {children}
        </label>
      );
    }

    if (type === 'select') {
      return <Select styles={selectCustomStyles} {...attributes}/>
    }

    // TODO: implement component type of "radio"
  }
}

CustomInput.propTypes = {
  className: t.string,
  type: t.oneOf(["checkbox", "radio", "select"]).isRequired,
  label: t.string,
  disabled: t.bool,
  iconColor: t.oneOf(["grey", "green", "red", "white"])
};

export default CustomInput;

