import React from "react";
import { shallow } from "enzyme";

import Icon from "./index";

describe("<Icon/>", () => {
  it("should render without crashing", () => {
    const wrapper = shallow(<Icon>someIcon</Icon>);

    expect(wrapper.html()).toBe(
      '<span class="sw-icon" style="width:24px;height:24px">someIcon</span>'
    );
  });
});
