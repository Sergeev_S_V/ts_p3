import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";

import "./Icon.scss";


const Icon = ({size, sizeUnit, width, height, centered, color, tag: Tag, className, ...attributes}) => {
  const classes = classNames(
    "sw-icon",
    "sw-icon--" + color,
    centered && "sw-icon--centered",
    className
  );

  const inlineStyles = {
    width: `${(size || width) + sizeUnit}`,
    height: `${(size || height) + sizeUnit}`
  };

  return (<Tag className={classes} style={inlineStyles} {...attributes}/>);
};

Icon.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
  centered: t.bool,
  width: t.number,
  height: t.number,
  size: t.number.isRequired,
  sizeUnit: t.string.isRequired,
  color: t.oneOf(["grey", "green", "red", "white"])
};

Icon.defaultProps = {
  tag: "span",
  size: 24,
  sizeUnit: "px",
  color: "grey"
};

export default Icon;
