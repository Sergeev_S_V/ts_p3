import React from "react";
import classNames from "classnames";

import {utils} from "../../internal";
import './Title.scss';


const Title = ({tag: Tag, className, ...attributes}) => {
  const classes = classNames("sw-title", className);

  return (<Tag className={classes} {...attributes}/>);
};

Title.propTypes = {
  tag: utils.tagPropType
};

Title.defaultProps = {
  tag: 'h1'
};

export default Title;
