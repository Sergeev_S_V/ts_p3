import React from "react";
import {mount} from "enzyme";

import Title from "./index";


describe("<Title/>", () => {

  const div = document.createElement('div');

  it("should render without crashing", () => {
    const wrapper = mount((<Title>Title</Title>), {attachTo: div});

    expect(wrapper.html()).toEqual('<h1 class="sw-title">Title</h1>');

    wrapper.unmount();
  });

});



