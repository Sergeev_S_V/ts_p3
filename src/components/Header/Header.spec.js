import React from "react";
import { shallow, mount } from "enzyme";

import Header from "./index";

describe("<Header/>", () => {
  const div = document.createElement("div");

  it("should render without crashing", () => {
    const wrapper = shallow(<Header />, { attachTo: div });

    expect(wrapper.html()).toEqual('<header class="l-header"></header>');

    wrapper.unmount();
  });

  it("should render with <HeaderInner/> without crashing", () => {
    const wrapper = mount(
      <Header>
        <Header.Inner tag="nav">HeaderInner</Header.Inner>
      </Header>,
      { attachTo: div }
    );

    expect(
      wrapper
        .find(".l-header__inner")
        .first()
        .html()
    ).toEqual('<nav class="l-header__inner">HeaderInner</nav>');

    wrapper.unmount();
  });
});
