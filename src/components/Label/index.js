import React from "react";
import classNames from "classnames";
import t from "prop-types";

import {utils} from "../../internal";

import "./Label.scss";

const Label = ({
  className,
  tag: Tag,
  check,
  for: htmlFor,
  ...attributes
}) => {
  const classes = classNames(
    "sw-label",
    check && "sw-label__checkbox",
    className
  );

  return (
    <Tag className={classes} htmlFor={htmlFor} {...attributes}/>
  );
};

Label.propTypes = {
  className: t.string,
  tag: utils.tagPropType,
  check: t.bool
};

Label.defaultProps = {
  tag: "label"
};

export default Label;
