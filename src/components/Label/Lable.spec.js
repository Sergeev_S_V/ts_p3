import React from "react";
import { shallow } from "enzyme";
import Label from "./";

describe("<Label />", () => {
  it("should render label element with class sw-label", () => {
    const wrapper = shallow(<Label content="Label text" />);

    expect(wrapper.html()).toBe(
      '<label class="sw-label">Label text</label>'
    );
  });
});
