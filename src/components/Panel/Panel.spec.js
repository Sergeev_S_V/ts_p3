import React from "react";
import {shallow, mount} from "enzyme";

import Panel from "./index";


describe("<Panel/>", () => {

  const div = document.createElement('div');

  it("should render without crashing", () => {
    const wrapper = shallow(
      (<Panel/>),
      {attachTo: div}
    );

    expect(wrapper.html()).toEqual('<div></div>');

    wrapper.unmount();
  });

  it("should render with <PanelLinks/> without crashing", () => {
    const wrapper = mount(
      (<Panel>
        <Panel.Links/>
      </Panel>),
      {attachTo: div}
    );

    expect(wrapper.find(".c-panel-links").first().html()).toEqual('<div class="c-panel-links"></div>');

    wrapper.unmount();
  });

  it("should render with children", () => {
    const _pathToIcon = "/path/to/icon.svg";
    const _text = "testing";
    const _alt = "alt testing";
    const wrapper = mount(
      (<Panel>
        <Panel.Links>
          <Panel.Icon icon={_pathToIcon} alt={_alt}/>
          <Panel.Text text={_text}/>
        </Panel.Links>
      </Panel>),
      {attachTo: div}
    );

    expect(wrapper.find(".c-panel-links__icon > img").first().html()).toEqual(`<img src="${_pathToIcon}" alt="${_alt}">`);

    expect(wrapper.find(".c-panel-links__text").first().html()).toEqual(`<p class="c-panel-links__text">${_text}</p>`);

    wrapper.unmount();
  });

});
