import React from "react";
import { shallow } from "enzyme";
import Row from "./";

describe("<Row />", () => {
  it("should render div element with class sw-row", () => {
    const wrapper = shallow(<Row />);

    expect(wrapper.html()).toBe('<div class="sw-row"></div>');
  });

  it("should render div element with class sw-row--height-100", () => {
    const wrapper = shallow(<Row allHeight />);

    expect(wrapper.hasClass("sw-row")).toBe(true);
  });

  it("should render div element with children", () => {
    const wrapper = shallow(<Row>children</Row>);

    expect(wrapper.html()).toBe('<div class="sw-row">children</div>');
  });

  it("should render div element with custom tag nav", () => {
    const wrapper = shallow(
      <Row tag="nav" allHeight>
        children
      </Row>
    );

    expect(wrapper.type()).toBe("nav");
    expect(wrapper.text()).toBe("children");
  });
});
