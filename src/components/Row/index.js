import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";

import "./Row.scss";

const Row = ({tag: Tag, className, fluid, ...attributes}) => {

  const classes = classNames(
    'sw-row',
    fluid && 'sw-row--fluid',
    className
  );

  return <Tag className={classes} {...attributes} />;
};

Row.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
  fluid: t.bool
};

Row.defaultProps = {
  tag: "div"
};

export default Row;
