import React from 'react';
import {mount} from "enzyme";

import ErrorBoundary from './index';

describe('<ErrorBoundary/>', () => {

  const div = document.createElement('div');

  it('should render without crashing', () => {
    const wrapper = mount((<ErrorBoundary>Hello</ErrorBoundary>), {attachTo: div});
    wrapper.unmount();
  });

});
