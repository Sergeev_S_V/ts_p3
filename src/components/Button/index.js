import React from "react";
import classNames from "classnames";
import t from "prop-types";

import {utils} from "../../internal";

import ButtonLoader from "./ButtonLoader";
import ButtonLabel from "./ButtonLabel";

import "./Button.scss";

const Button = ({
  tag: Tag,
  label,
  children,
  className,
  onClick,
  color,
  outlined,
  loading,
  fluid,
  icon,
  ...attributes
}) => {
  const classes = classNames(
    "sw-button",
    `sw-button--${color}` + ((outlined && "--outline") || "") + ((attributes.disabled && "--disabled") || ""),
    icon && "sw-button__icon",
    fluid && "sw-button--fluid",
    className
  );

  return (
    <Tag className={classes}
         onClick={attributes.disabled ? null : onClick}
         {...attributes}>
      {loading && <ButtonLoader color={outlined ? "darkblue" : "white"}/>}
      {label ? <ButtonLabel label={label}/> : children}
    </Tag>
  );
};

Button.propTypes = {
  tag: utils.tagPropType,
  label: utils.tagPropType,
  className: t.string,
  onClick: t.func,
  color: t.oneOf(["default", "danger", "violet"]),
  disabled: t.bool,
  outlined: t.bool,
  loading: t.bool,
  fluid: t.bool,
  icon: t.bool,
};

Button.defaultProps = {
  tag: "button",
  color: "default",
  disabled: false,
  outlined: false,
  loading: false,
  fluid: false
};

Button.Loader = ButtonLoader;
Button.Label = ButtonLabel;

export default Button;
