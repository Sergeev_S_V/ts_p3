import React from "react";
import classNames from "classnames";
import t from "prop-types";

import { utils } from "../../internal";

const ButtonLabel = ({
  tag: Tag,
  className,
  label,
  children,
  ...attributes
}) => {
  const classes = classNames("sw-button__label", className);

  return (
    <Tag className={classes} {...attributes}>
      {label || children}
    </Tag>
  );
};

ButtonLabel.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
  label: utils.tagPropType
};

ButtonLabel.defaultProps = {
  tag: "span"
};

export default ButtonLabel;
