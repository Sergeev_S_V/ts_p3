import React from "react";
import sinon from "sinon";
import { shallow } from "enzyme";
import Button from "./";

describe("<Button />", () => {
  //#region ***default button***
  it("should render button element with class sw-button sw-button--default", () => {
    const wrapper = shallow(<Button label="Default" />);

    expect(wrapper.html()).toBe(
      '<button class="sw-button sw-button--default"><span class="sw-button__label">Default</span></button>'
    );
  });

  it("should render button element with class sw-button sw-button--default with loading state", () => {
    const wrapper = shallow(<Button label="Default loading" loading />);

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--default")).toBe(true);
    expect(wrapper.find("button").children()).toHaveLength(2);
  });

  it("should render button element with class sw-button sw-button--default-outline", () => {
    const wrapper = shallow(<Button label="Default outline" outlined />);

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--default--outline")).toBe(true);
  });

  it("should render button element with class sw-button sw-button--default sw-button--fluid", () => {
    const wrapper = shallow(<Button label="Default fluid" fluid />);

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--default")).toBe(true);
    expect(wrapper.hasClass("sw-button--fluid")).toBe(true);
  });

  // #endregion ***default button***

  //#region ***default disabled button***
  it("should render button element with class sw-button sw-button--default--disabled", () => {
    const wrapper = shallow(<Button label="disabled Default" disabled />);

    expect(wrapper.hasClass("sw-button--default--disabled")).toBe(true);
  });

  it("should render button element with class sw-button sw-button--default--disabled with loading state", () => {
    const wrapper = shallow(
      <Button label="disabled Default loading" loading disabled />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--default--disabled")).toBe(true);
    expect(wrapper.find("button").children()).toHaveLength(2);
  });

  it("should render button element with class sw-button sw-button--default--outline--disabled", () => {
    const wrapper = shallow(
      <Button label="disabled Default outline" outlined disabled />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--default--outline--disabled")).toBe(
      true
    );
  });

  it("should render button element with class sw-button sw-button--default--disabled sw-button--fluid", () => {
    const wrapper = shallow(
      <Button label="disabled Default fluid" fluid disabled />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--default--disabled")).toBe(true);
    expect(wrapper.hasClass("sw-button--fluid")).toBe(true);
  });

  // #endregion ***default disabled button***

  //#region  ***danger button***

  it("should render button element with class sw-button sw-button--danger", () => {
    const wrapper = shallow(<Button label="danger" color="danger" />);

    expect(wrapper.hasClass("sw-button--danger")).toBe(true);
  });

  it("should render button element with class sw-button sw-button--danger with loading state", () => {
    const wrapper = shallow(
      <Button label="danger loading" loading color="danger" />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--danger")).toBe(true);
    expect(wrapper.find("button").children()).toHaveLength(2);
  });

  it("should render button element with class sw-button sw-button--danger--outline", () => {
    const wrapper = shallow(
      <Button label="danger outline" outlined color="danger" />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--danger--outline")).toBe(true);
  });

  it("should render button element with class sw-button sw-button--danger sw-button--fluid", () => {
    const wrapper = shallow(
      <Button label="danger fluid" fluid color="danger" />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--danger")).toBe(true);
    expect(wrapper.hasClass("sw-button--fluid")).toBe(true);
  });

  //#endregion  ***danger button***

  //#region  ***danger disabled button***

  it("should render button element with class sw-button sw-button--danger--disabled", () => {
    const wrapper = shallow(
      <Button label="disabled danger" disabled color="danger" />
    );

    expect(wrapper.hasClass("sw-button--danger--disabled")).toBe(true);
  });

  it("should render button element with class sw-button sw-button--danger--disabled with loading state", () => {
    const wrapper = shallow(
      <Button label="disabled danger loading" loading disabled color="danger" />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--danger--disabled")).toBe(true);
    expect(wrapper.find("button").children()).toHaveLength(2);
  });

  it("should render button element with class sw-button sw-button--danger--outline--disabled", () => {
    const wrapper = shallow(
      <Button
        label="disabled danger outline"
        outlined
        disabled
        color="danger"
      />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--danger--outline--disabled")).toBe(true);
  });

  it("should render button element with class sw-button sw-button--danger--disabled sw-button--fluid", () => {
    const wrapper = shallow(
      <Button label="disabled danger fluid" fluid disabled color="danger" />
    );

    expect(wrapper.hasClass("sw-button")).toBe(true);
    expect(wrapper.hasClass("sw-button--danger--disabled")).toBe(true);
    expect(wrapper.hasClass("sw-button--fluid")).toBe(true);
  });

  //#endregion  ***danger disabled button***

  // #region CHECK events
  it("simulate button element click", () => {
    const onButtonClick = sinon.spy();
    const wrapper = shallow(<Button onClick={onButtonClick} />);

    wrapper.find("button").simulate("click");
    expect(onButtonClick).toHaveProperty("callCount", 1);
  });
  // #endregion
});
