import React from "react";
import classNames from "classnames";
import t from "prop-types";

import {utils} from "../../internal";

const ListItem = ({tag: Tag, className, ...attributes}) => {
  const classes = classNames("sw-list__item", className);

  return <Tag className={classes} {...attributes} />;
};

ListItem.propTypes = {
  className: t.string,
  tag: utils.tagPropType
};

ListItem.defaultProps = {
  tag: 'li'
};

export default ListItem;
