import React from "react";
import {shallow, mount} from "enzyme";

import List from "./index";


describe("<List/>", () => {

  const div = document.createElement('div');

  it("should render without crashing", () => {
    const wrapper = shallow((<List/>), {attachTo: div});

    expect(wrapper.html()).toEqual('<ul class="sw-list"></ul>');

    wrapper.unmount();
  });

  it("should render with children", () => {
    const wrapper = mount(
      (<List>
        <List.Item>1</List.Item>
        <List.Item>2</List.Item>
        <List.Item>3</List.Item>
      </List>),
      {attachTo: div}
    );

    expect(wrapper.find('.sw-list__item')).toHaveLength(3);

    expect(wrapper.find('.sw-list__item').first().text()).toEqual('1');

    expect(wrapper.find('.sw-list__item').last().text()).toEqual('3');

    wrapper.unmount();
  });

});
