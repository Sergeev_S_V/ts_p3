import React from "react";
import {shallow, mount} from "enzyme";

import Navigation from "./index";


describe("<Navigation/>", () => {

  const div = document.createElement('div');

  it("should render without crashing", () => {
    const wrapper = shallow((<Navigation/>), {attachTo: div});

    expect(wrapper.html()).toEqual('<nav class="l-navigation"></nav>');

    wrapper.unmount();
  });

  it("should render with <NavigationHeader/> without crashing", () => {
    const wrapper = mount(
      (<Navigation>
        <Navigation.Header>Header</Navigation.Header>
      </Navigation>),
      {attachTo: div}
    );

    expect(wrapper.find(".l-navigation__heading").first().html()).toEqual('<header class="l-navigation__heading">Header</header>');

    wrapper.unmount();
  });

  it("should render with <NavigationInner/> without crashing", () => {
    const wrapper = mount(
      (<Navigation>
        <Navigation.Inner>Inner</Navigation.Inner>
      </Navigation>),
      {attachTo: div}
    );

    expect(wrapper.find(".l-navigation__inner").first().html()).toEqual('<div class="l-navigation__inner">Inner</div>');

    wrapper.unmount();
  });

  it("should render with <NavigationBody/> without crashing", () => {
    const wrapper = mount(
      (<Navigation>
        <Navigation.Body>Body</Navigation.Body>
      </Navigation>),
      {attachTo: div}
    );

    expect(wrapper.find(".l-navigation__body").first().html()).toEqual('<div class="l-navigation__body">Body</div>');

    wrapper.unmount();
  });

});
