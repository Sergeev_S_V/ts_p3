import React, { Component } from "react";
import classNames from "classnames";
import t from "prop-types";

import { utils } from "../../internal";

import "./Popup.scss";

class Popup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      opened: false
    };
  }

  handleClick = e => {
    this.setState({
      opened: !this.state.opened
    });
  };

  render() {
    const {
      tag: Tag,
      className,
      children,
      wrapperClassName,
      text,
      ...attributes
    } = this.props;
    const { opened } = this.state;

    const classes = classNames("popup__header", opened && "open", className);
    const wrapperClasses = classNames("popup", wrapperClassName);

    return (
      <Tag
        onClick={this.handleClick}
        onFocus={this.handleClick}
        onMouseEnter={this.handleClick}
        onMouseLeave={this.handleClick}
        className={wrapperClasses}
        {...attributes}
      >
        <div className={classes}>
          <span className="popup__arrow" />
          {text}
        </div>
        {children}
      </Tag>
    );
  }
}

Popup.propTypes = {
  tag: utils.tagPropType,
  className: t.string,
  wrapperClassName: t.string,
  text: t.string
};

Popup.defaultProps = {
  tag: "div"
};

export default Popup;
