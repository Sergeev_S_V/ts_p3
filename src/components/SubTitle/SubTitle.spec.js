import React from "react";
import {mount} from "enzyme";

import Subtitle from "./index";


describe("<SubTitle>", () => {

  const div = document.createElement('div');

  it("should render without crashing", () => {
    const wrapper = mount((<Subtitle>SubTitle</Subtitle>), {attachTo: div});

    expect(wrapper.html()).toEqual('<h2 class="sw-subtitle">SubTitle</h2>');

    wrapper.unmount();
  });

});

