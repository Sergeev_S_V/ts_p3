import React from "react";
import { shallow } from "enzyme";

import Form from "./";
import { Label, Input } from "../";

describe("<Form />", () => {
  it("should render form element with class sw-form", () => {
    const wrapper = shallow(<Form />);

    expect(wrapper.html()).toBe('<form class="sw-form"></form>');
  });

  it("should render form element with class sw-form and children", () => {
    const wrapper = shallow(
      <Form>
        <Label content="Search" for="search" />
        <Input id="search" name="search" placeholder="Input some to search" />
      </Form>
    );

    expect(wrapper.contains(<Label content="Search" for="search" />)).toEqual(
      true
    );
    expect(
      wrapper.contains(
        <Input id="search" name="search" placeholder="Input some to search" />
      )
    ).toEqual(true);
  });

  it("should render form element with class sw-form and children", () => {
    const wrapper = shallow(
      <Form>
        <Form.Group>
          <Label content="User name" for="user_name" />
          <Input
            id="user_name"
            name="user_name"
            placeholder="Input your user name"
          />
        </Form.Group>
        <Form.Group>
          <Label content="Password" for="password" />
          <Input
            id="password"
            name="password"
            placeholder="Input your password"
          />
        </Form.Group>
        <Input type="submit" value="Send" />
      </Form>
    );

    expect(wrapper.html()).toEqual(
      '<form class="sw-form"><div><label class="sw-label" for="user_name">User name</label><input type="text" class="sw-input sw-input--default" id="user_name" name="user_name" placeholder="Input your user name"/></div><div><label class="sw-label" for="password">Password</label><input type="text" class="sw-input sw-input--default" id="password" name="password" placeholder="Input your password"/></div><input type="submit" class="sw-input sw-input--default" value="Send"/></form>'
    );
  });
});
