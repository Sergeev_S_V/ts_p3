import React from "react";
import {shallow} from "enzyme";
import Column from "./";

describe("<Column />", () => {
  it("should render div element with class sw-col", () => {
    const wrapper = shallow(<Column/>);

    expect(wrapper.html()).toBe('<div class="sw-col"></div>');
  });

  it("should render div element with class fluid", () => {
    const wrapper = shallow(<Column fluid/>);

    expect(wrapper.hasClass("sw-col-fluid")).toBe(true);
  });

  it("should render div element with dominated class fluid", () => {
    const wrapper = shallow(<Column col={5} fluid/>);

    expect(wrapper.hasClass("sw-col-fluid")).toBe(true);
  });

  it("should render div element with children", () => {
    const wrapper = shallow(<Column>children</Column>);

    expect(wrapper.html()).toBe('<div class="sw-col">children</div>');
  });

  it("should render div element with class sw-col-5", () => {
    const wrapper = shallow(<Column col={5}/>);

    expect(wrapper.hasClass("sw-col-5")).toBe(true);
  });

  it("should render div element with custom tag main, class sw-col and children", () => {
    const wrapper = shallow(<Column tag="main">Hello!</Column>);

    expect(wrapper.hasClass("sw-col")).toBe(true);
    expect(wrapper.text()).toBe("Hello!");
    expect(wrapper.type()).toBe("main");
  });
});
