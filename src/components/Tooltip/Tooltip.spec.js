import React from "react";
import { shallow } from "enzyme";
import { Tooltip, Label } from "../";

describe("<Tooltip />", () => {
  it("should render div element with class sw-tooltip", () => {
    const wrapper = shallow(
      <Tooltip
        text="Tooltip"
        position="right"
        trigger={<Label>trigger</Label>}
      />
    );

    expect(wrapper.html()).toBe(
      '<div class="tooltip"><label class="sw-label">trigger</label></div>'
    );
  });

  it("should render div element with class sw-tooltip simulate mouseOver", () => {
    const wrapper = shallow(
      <Tooltip
        text="Tooltip"
        position="right"
        trigger={<Label>trigger</Label>}
      />
    );

    wrapper.simulate("mouseover");
    expect(wrapper.state("open")).toBe(true);

    wrapper.simulate("mouseout");
    expect(wrapper.state("open")).toBe(false);
  });
});
