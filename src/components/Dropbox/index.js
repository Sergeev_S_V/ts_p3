import React from 'react';
import classNames from 'classnames';
import t from 'prop-types';

import {utils} from '../../internal';

import './Dropbox.scss';

const Dropbox = ({tag: Tag, className, ...attributes}) => {
  const classes = classNames('sw-dropbox', className);

  return(
    <Tag className={classes} {...attributes} />
  );
};

Dropbox.propTypes = {
  tag: utils.tagPropType,
  className: t.string
};

Dropbox.defaultProps = {
  tag: 'div'
};

export default Dropbox;
