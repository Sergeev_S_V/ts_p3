import React from "react";
import {shallow, mount} from "enzyme";

import Rank from "./index";


describe("<Rank/>", () => {

  const div = document.createElement('div');

  it("should render without crashing", () => {
    const wrapper = shallow((<Rank/>), {attachTo: div});

    expect(wrapper.hasClass("sw-rank")).toBe(true);

    wrapper.unmount();
  });

  it("should render with level and color", () => {
    function runTestWithLevelAndColorOf(_level, _color) {
      const wrapper = mount((<Rank level={_level} color={_color}/>), {attachTo: div});

      // TODO: check color test
      expect(wrapper.text()).toEqual(`${_level}`);

      wrapper.unmount();
    }

    runTestWithLevelAndColorOf(1, "green");
    runTestWithLevelAndColorOf(3, "yellow");
    runTestWithLevelAndColorOf(5, "red");
    // with custom color
    runTestWithLevelAndColorOf(5, "#4336d4");
  });

});
