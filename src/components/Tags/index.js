import React from "react";
import t from "prop-types";
import classNames from "classnames";

import {utils} from "../../internal";
import TagItem from "./TagItem";
import "./Tags.scss";

const Tags = ({tag: Tag, className, ...attributes}) => {
  const classes = classNames("sw-tags", className);

  return <Tag className={classes} {...attributes} />;
};

Tags.propTypes = {
  className: t.string,
  tag: utils.tagPropType
};

Tags.defaultProps = {
  tag: "ul"
};

Tags.Item = TagItem;

export default Tags;
