import React from "react";
import {shallow, mount} from "enzyme";

import Tags from "./index";

describe("<Tags/>", () => {

  const sampleTags = [
    "Javascript",
    "Java",
    "CSS",
    "Ngnix",
    "SQL",
    "Visual studio",
    "C#",
    "Python",
    "Microsoft",
    "Linux",
    "Ubuntu",
    "React"
  ];

  const div = document.createElement('div');

  it("should render without crashing", () => {
    const wrapper = shallow(
      (<Tags/>),
      {attacheTo: div}
    );
    expect(wrapper.html()).toEqual('<ul class="sw-tags"></ul>');
    wrapper.unmount();
  });

  it("should render with children", () => {
    const wrapper = mount(
      (<Tags>
        {sampleTags.map((tag, idx) => (<Tags.Item key={idx}>{tag}</Tags.Item>))}
      </Tags>),
      {attacheTo: div}
    );

    expect(wrapper.find('.sw-tags__item')).toHaveLength(sampleTags.length);
    expect(wrapper.find('.sw-tags__item').first().html()).toEqual('<li class="sw-tags__item">Javascript</li>');
    expect(wrapper.find('.sw-tags__item').last().html()).toEqual('<li class="sw-tags__item">React</li>');

    wrapper.unmount();
  });

});
