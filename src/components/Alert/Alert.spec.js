import React from "react";
import renderer from 'react-test-renderer';

import Alert from './index';
import {Icon, MaterialIcon} from "../index";


describe('<Alert/>', () => {
  it('renders itself with default props', () => {
    const component = renderer.create(<Alert content='alert'/>).toJSON();
    expect(component).toMatchSnapshot();
  });

  it('renders itself with props were passed', () => {
    const component = renderer.create(
      <Alert
        content='alert'
        color='error'
        blockClass='block'
        titleClass='title'
        icon={<Icon><MaterialIcon name="check_circle"/></Icon>}
      />
    ).toJSON();
    expect(component).toMatchSnapshot();
  });
});
