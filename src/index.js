import 'react-app-polyfill/ie9'; // For IE 9-11 support
import 'react-app-polyfill/ie11'; // For IE 11 support
import './polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'mobx-react';
import axios from 'axios';

import * as serviceWorker from './serviceWorker';
import {RootStore, apiV1, KEYS, utils, LocalStorage} from './internal';

import "react-datepicker/dist/react-datepicker.css";
import './index.scss';
import App from './App';

// create root store
export const rootStore = RootStore.create(
  // initial values
  {
    authStore: {},
    signinStore: {},
    signupStore: {},
    appStore: {},
    problemStore: {},
    problemsUIStore: {},
    problemSubmissionsStore: {},
    problemSubmissionsUIStore: {},
    controlProblemUIStore: {},
    tagsStore: {},
    tagsUIStore: {}
  },
  // injected env dependencies
  {
    apiV1,
    axios,
    KEYS,
    LocalStorage
  }
);

utils.persist(
  KEYS.LS_KEY_PERSIST_AUTH_STORE,
  rootStore.authStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {currentUser: true}
);
utils.persist(
  KEYS.LS_KEY_PERSIST_APP_STORE,
  rootStore.appStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {currentRole: true}
);
utils.persist(
  KEYS.LS_KEY_PERSIST_PROBLEMS_UI_STORE,
  rootStore.problemsUIStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {filter: true}
);
utils.persist(
  KEYS.LS_KEY_PERSIST_PROBLEM_SUBMISSIONS_UI_STORE,
  rootStore.problemSubmissionsUIStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {filter: true}
);


apiV1.interceptors.request.use(config => {
  config.headers['Authorization'] =
    rootStore.authStore.currentUser.token_type + ' ' + rootStore.authStore.currentUser.access_token;
  return config;
});

ReactDOM.render(
  <Provider stores={rootStore}>
    <App/>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
